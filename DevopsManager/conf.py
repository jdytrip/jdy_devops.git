import os
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'devops',
        'USER': 'root',
        'PASSWORD': '{}'.format(os.getenv('MYSQL_PASSWORD')),  # 填写密码
        'HOST': 'db',
        'PORT': '3306',
        'CONN_MAX_AGE': 3,
    }
}

# CELERY_REDIS = "redis://:密码@192.168.0.1:7082/7"
CELERY_REDIS = "redis://redis:6379/5"  # docker版无需修改
CACHE_REDIS = "redis://redis:6379/7"  # docker版无需修改


LINSTEN_ADDR='http://0.0.0.0:8000'

# 应用分发存储日志路径
DAEMON_LOG_PATH = '/src/yokiy-devops/logs/daemon_log'
# 业务代码生成日志，日志挂在点目录
SERVER_LOG_PATH = '/src/yokiy-devops/logs/test_log'

# 发送邮件相关配置
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'  #email后端
EMAIL_USE_TLS = False   #是否使用TLS安全传输协议
EMAIL_USE_SSL = True    #是否使用SSL加密，qq企业邮箱要求使用
EMAIL_HOST = ''   #发送邮件的邮箱 的 SMTP服务器，这里用了qq企业邮箱
EMAIL_PORT = 465     #发件箱的SMTP服务器端口
EMAIL_HOST_USER = ''    #发送邮件的邮箱地址
EMAIL_HOST_PASSWORD = ''         #发送邮件的邮箱密码

# 构建服务器ip
BUILD_SERVER_IP="127.0.0.1"


LOGIN_URL = '/common/account/login/'
PROJECT_URL = '/server/project/'
PROJECT_ID_URL = PROJECT_URL + '{project_id}/'
PROJECT_ID_API_URL = PROJECT_ID_URL + 'api/'
PROJECT_ID_APP_ID_URL = PROJECT_ID_URL + 'app/{app_id}/'

