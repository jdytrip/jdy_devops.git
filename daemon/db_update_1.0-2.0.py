import os,sys
import django

def main():
    # 新增关于db执行权限
    sys_p = GlobalAuthority.objects.get(name='系统管理', url="", type=GlobalAuthority.SYS)
    GlobalAuthority.objects.create(name='数据库管理', url='/admin/db/', type=GlobalAuthority.SYS, parent=sys_p)
    GlobalAuthority.objects.filter(url__startswith='/server/db/').delete()
    try:
        p_level_one = GlobalAuthority.objects.get(name='应用数据库管理', url='/server/db/', type=GlobalAuthority.SERVER)

    except:
        p_level_one = GlobalAuthority.objects.create(name='应用数据库管理', url='/server/db/', type=GlobalAuthority.SERVER)

    # 轮询project
    projects = ProjectModel.objects.all()
    # print(projects)
    for proj_obj in projects.iterator():
        # path('/server/db/<int:proj_id>/list/', views.db_list, name='db_list'),
        # path('/server/db/<int:proj_id>/<int:db_id>/add/', views.sql_edit, name='sql_edit'),
        # path('/server/db/<int:proj_id>/<int:db_id>/list/', views.sql_list, name='sql_list'),
        # path('/server/db/<int:proj_id>/<int:db_id>/examine/<int:sql_id>/', views.sql_examine, name='sql_examine'),
        # path('/server/db/<int:proj_id>/<int:db_id>/detail/<int:sql_id>/', views.sql_detail, name='sql_detail'),
        pid = proj_obj.id
        p_level_two = GlobalAuthority.objects.create(name=proj_obj.project_name, parent=p_level_one,
                                                     url='/server/db/{}/'.format(pid),
                                                     type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj)
        # # '/server/db/{}/list/'
        GlobalAuthority.objects.create(name='显示项目所属db', url='/server/db/{}/list/'.format(pid),
                                        parent=p_level_two, type=GlobalAuthority.PROJECT,
                                        link_project=proj_obj)
        for db in DbNode.objects.filter(project_id=pid):
            base_url = '/server/db/{}/{}/'.format(pid,db.id)
            p_level_three = GlobalAuthority.objects.create(name=db.name, url=base_url,
                                            parent=p_level_two, type=GlobalAuthority.PROJECT,
                                            link_db=db)
            son_list = []
            son_list.append(GlobalAuthority(name='新增sql', url=base_url+'add/',
                                            parent=p_level_three, type=GlobalAuthority.PROJECT,
                                            link_db=db))
            son_list.append(GlobalAuthority(name='显示db所属sql', url=base_url+'list/',
                                            parent=p_level_three, type=GlobalAuthority.PROJECT,
                                            link_db=db))
            son_list.append(GlobalAuthority(name='审核db所属sql', url=base_url+'examine/',
                                            parent=p_level_three, type=GlobalAuthority.PROJECT,
                                            link_db=db))
            son_list.append(GlobalAuthority(name='查看sql', url=base_url +'detail/',
                                            parent=p_level_three, type=GlobalAuthority.PROJECT,
                                            link_db=db))
            son_list.append(GlobalAuthority(name='审核查看sql', url=base_url + 'select/',
                                            parent=p_level_three, type=GlobalAuthority.PROJECT,
                                            link_db=db))
            GlobalAuthority.objects.bulk_create(son_list)

    return
    # 删除PROJECT重复权限数据
    for app in AppModel.objects.all():
        project_id, app_id = app.app_project.id, app.id
        # url = '/server/project/{}/app/{}/index/'.format(project_id, app_id)
        new_url = '/server/project/{}/app/{}/detail/'.format(project_id, app_id)
        add_url = '/server/project/{}/app/{}/log/'.format(project_id, app_id)
        try:
            g = GlobalAuthority.objects.get(url__contains=new_url)
            log_nodes = GlobalAuthority.objects.filter(parent=g.parent,link_app=app,type=GlobalAuthority.APP,url=add_url)
            number = len( log_nodes)
            if number == 1 :
                print('只有一个')
                continue
            elif number == 0:
                print('没有创建')
                log_node = GlobalAuthority.objects.create(name='日志', url=add_url,
                                                          parent=g.parent, type=GlobalAuthority.APP,
                                                          link_app=app)
                for ut in UserAuthority.objects.filter(authority=g):
                    UserAuthority.objects.create(authority=log_node, user=ut.user)
            elif number > 1:
                print('超过一个')
                for node in log_nodes[1:]:
                    node.delete()
            # g.url = new_url
            # g.save()
        except Exception as e:
            print('异常：{}'.format( e.args))
    return



    # 删除PROJECT对应权限中多余的权限
    GlobalAuthority.objects.filter(url__endswith='app/list/').delete()
    GlobalAuthority.objects.filter(url__contains='')
    for app in AppModel.objects.all():
        project_id,app_id = app.app_project.id, app.id
        url = '/server/project/{}/app/{}/index/'.format(project_id,app_id)
        new_url = '/server/project/{}/app/{}/detail/'.format(project_id,app_id)
        add_url = '/server/project/{}/app/{}/log/'.format(project_id,app_id)
        try:
            g = GlobalAuthority.objects.get(url__contains=url)
            log_node = GlobalAuthority.objects.create(name='日志', url=add_url,
                                                      parent=g.parent, type=GlobalAuthority.APP,
                                                      link_app=app)
            for ut in UserAuthority.objects.filter(authority=g):
                UserAuthority.objects.create(authority=log_node, user=ut.user)

            g.url = new_url
            g.save()
        except Exception as e:
            print('APP为{} 报错，{} '.format(app.app_name, e.args))
    return
    User.objects.all().update(is_active=True)

    from server import tasks
    tasks.update_db_authority.delay()
    tasks.update_cache_authority.delay()
    return

    from server import tasks
    # 更新权限数据库记录
    tasks.update_db_authority.delay()
    # 根据权限表数据刷新缓存中权限数据
    tasks.update_cache_authority.delay()

    User.objects.all().update(is_active=True)


    return

    UserAuthority.objects.all().delete()
    GlobalAuthority.objects.all().delete()
    common_p = GlobalAuthority.objects.create(name='通用管理',url="",type=GlobalAuthority.COMMON)
    GlobalAuthority.objects.create(name='修改个人信息',url='/common/account/change/',type=GlobalAuthority.COMMON,parent=common_p)
    GlobalAuthority.objects.create(name='登录',url='/common/account/login/',type=GlobalAuthority.COMMON,parent=common_p)
    GlobalAuthority.objects.create(name='登出',url='/common/account/logout/',type=GlobalAuthority.COMMON,parent=common_p)

    sys_p = GlobalAuthority.objects.create(name='系统管理',url="",type=GlobalAuthority.SYS)
    GlobalAuthority.objects.create(name='项目管理',url='/admin/project/',type=GlobalAuthority.SYS,parent=sys_p)
    GlobalAuthority.objects.create(name='用户管理',url='/admin/account/',type=GlobalAuthority.SYS,parent=sys_p)

    p_level_one = GlobalAuthority.objects.create(name='服务管理',url=settings.PROJECT_URL,type=GlobalAuthority.SERVER)
    try:
        user = User.objects.get(username='admin')
    except:
        user = User.objects.create_superuser(username='admin', password='admin123', email='admin@admin.com')
    for au in GlobalAuthority.objects.filter():
        UserAuthority.objects.create(user=user, authority=au)

    # 轮询project
    projects = ProjectModel.objects.all()
    for proj_obj in projects.iterator():

        project_url = settings.PROJECT_ID_URL.format(project_id=proj_obj.id)
        p_level_two = GlobalAuthority.objects.create(name=proj_obj.project_name, parent=p_level_one,
                                                     url=project_url, type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj)
        api_url = project_url + 'api/'
        p_level_three = GlobalAuthority.objects.create(name='api监控', parent=p_level_two,
                                                       url=api_url, type=GlobalAuthority.PROJECT,
                                                       link_project=proj_obj)
        son_list = []
        son_list.append(GlobalAuthority(name='删除指定监控项', url=api_url + 'delete/',
                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                        link_project=proj_obj))
        son_list.append(GlobalAuthority(name='查看所有监控项', url=api_url + 'index/',
                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                        link_project=proj_obj))
        son_list.append(GlobalAuthority(name='修改或新增指定监控项', url=api_url + 'edit/',
                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                        link_project=proj_obj))
        son_list.append(GlobalAuthority(name='新增app', url=project_url + 'app/add/',
                                        parent=p_level_two, type=GlobalAuthority.PROJECT,
                                        link_project=proj_obj))

        GlobalAuthority.objects.bulk_create(son_list)
        # 轮询project下app
        for app_node in AppModel.objects.filter(app_project=proj_obj):
            proj_parent = p_level_two
            app_url = settings.PROJECT_ID_APP_ID_URL.format(
                project_id=proj_obj.id, app_id=app_node.id)
            app_parent = GlobalAuthority.objects.create(name=app_node.app_name, url=app_url,
                                                        parent=proj_parent, type=GlobalAuthority.APP,
                                                        link_app=app_node)
            app_s_list = []
            for act in [{'name': '删除', 'url': 'delete'}, {'name': '增改', 'url': 'edit'},
                        {'name': '执行', 'url': 'task'}, {'name': '预览', 'url': 'index'}]:
                app_s_list.append(
                    GlobalAuthority(name=act['name'], url=app_url + act['url'] + '/',
                                    parent=app_parent, type=GlobalAuthority.APP,
                                    link_app=app_node)
                )
            GlobalAuthority.objects.bulk_create(app_s_list)


if __name__ == '__main__':
    current_path = os.path.dirname(
        os.path.split(os.path.realpath(__file__))[0])
    sys.path.append(current_path)
    os.environ['DJANGO_SETTINGS_MODULE'] = 'DevopsManager.settings'
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    from DevopsManager import settings
    from ManagerMaster.models import User
    from ManagerMaster.models import GlobalAuthority
    from ManagerMaster.models import UserAuthority
    from ManagerMaster.models import *
    main()

