#coding=utf-8
import os,sys
import django

def main():
    ### 删除所有的User用户
    # UserAuthority.objects.all().delete()
    ### 清空权限表
    # GlobalAuthority.objects.all().delete()

    try:
        user = User.objects.get(username='admin')
    except:
        common_p = GlobalAuthority.objects.create(name='通用管理', url="", type=GlobalAuthority.COMMON)
        GlobalAuthority.objects.create(name='修改个人信息', url='/common/account/change/', type=GlobalAuthority.COMMON,
                                       parent=common_p)
        GlobalAuthority.objects.create(name='登录', url='/common/account/login/', type=GlobalAuthority.COMMON,
                                       parent=common_p)
        GlobalAuthority.objects.create(name='登出', url='/common/account/logout/', type=GlobalAuthority.COMMON,
                                       parent=common_p)

        sys_p = GlobalAuthority.objects.create(name='系统管理', url="", type=GlobalAuthority.SYS)
        GlobalAuthority.objects.create(name='项目管理', url='/admin/project/', type=GlobalAuthority.SYS, parent=sys_p)
        GlobalAuthority.objects.create(name='用户管理', url='/admin/account/', type=GlobalAuthority.SYS, parent=sys_p)
        GlobalAuthority.objects.create(name='数据库管理', url='/admin/db/', type=GlobalAuthority.SYS, parent=sys_p)

        GlobalAuthority.objects.create(name='服务管理', url=settings.PROJECT_URL, type=GlobalAuthority.SERVER)
        GlobalAuthority.objects.create(name='应用数据库管理', url='/server/db/', type=GlobalAuthority.SERVER)
        user = User.objects.create_superuser(username='admin', password='admin123', email='admin@admin.com')
        user.is_active = True
        user.save()
        for au in GlobalAuthority.objects.filter():
            UserAuthority.objects.create(user=user, authority=au)

    from ManagerMaster import tasks
    # 更新权限数据库记录
    tasks.update_db_authority.delay()
    # 根据权限表数据刷新缓存中权限数据
    tasks.update_cache_authority.delay()

    # User.objects.all().update(is_active=True)



if __name__ == '__main__':
    current_path = os.path.dirname(
        os.path.split(os.path.realpath(__file__))[0])
    sys.path.append(current_path)
    os.environ['DJANGO_SETTINGS_MODULE'] = 'DevopsManager.settings'
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    from DevopsManager import settings
    from ManagerMaster.models import User
    from ManagerMaster.models import GlobalAuthority
    from ManagerMaster.models import UserAuthority
    from ManagerMaster.models import *
    main()

