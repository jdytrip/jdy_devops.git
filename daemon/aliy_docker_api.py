import sys,os
import json
import requests

CUR_PATH=os.path.split(
    os.path.realpath(__file__))[0]

class MyDockerControl():
    URL = "https://master4g9.cs-cn-hangzhou.aliyun.com:20161"
    PROJECT_URL = URL + "/projects/"
    KEY_PATH = os.path.join(CUR_PATH,'aliy_key')
    CA_FILE = os.path.join(KEY_PATH,'ca.pem')
    CERT_TUPPLE = (
        os.path.join(KEY_PATH,'cert.pem'),
        os.path.join(KEY_PATH,'key.pem')
    )

    def __init__(self,app_name):
        self.app_name = app_name

    def _get(self,url):
        return requests.get(url,verify=self.CA_FILE,
                            cert=self.CERT_TUPPLE)

    def _post(self,url):
        return requests.post(url,verify=self.CA_FILE,
                             cert=self.CERT_TUPPLE)

    def query_app(self,qk=['current_state']):
        '''验证app名称是否符合'''
        r = self._get(self.PROJECT_URL)
        ret = r.status_code
        if ret == 200:
            for d in json.loads(r.content):
                if self.app_name == d['name']:
                    log = ""
                    for q in qk:
                        log += ',{}'.format(d[q])
                    return True,log
            return False,'Not find app'
        else:
            return False,'Curl fail'

    def ops_app(self, action ):
        '''
        操作选项
        start：启动应用
        stop：停止应用
        redeploy：重新部署
        '''
        is_find, stat = self.query_app()
        URL = ""
        if is_find:
            if action == "stop" and stat == "running" :
                URL = self.PROJECT_URL + self.app_name + '/stop'
            elif action == "start" and stat == "stopped":
                URL = self.PROJECT_URL + self.app_name + '/start'
            elif action == "redeploy":
                URL = self.PROJECT_URL + self.app_name + '/redeploy'
        else:
            return 404,'App Not Find'
        if URL:
            r = self._post(URL)
            try:
                if r.status_code == 200:
                    return 200,r.content
                else:
                    return r.status_code,r.content
            except Exception as e:
                return 500,e.args[0]


