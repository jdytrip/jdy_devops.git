import os
import json,re,sys
from pytz import utc
import apscheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.events import EVENT_JOB_EXECUTED,EVENT_JOB_ERROR
from apscheduler.executors.pool import ThreadPoolExecutor
import threading
from datetime import datetime

import traceback
import pymysql
import datetime
import pymysql.err
import logging.handlers
logging.basicConfig(level=logging.WARNING)
MUTEX = threading.Lock()

SCHEDULER_JOB_ID = {
    'read' : 'readjob',
}

FILE_MD5_DICT = {}
MUTEX = threading.Lock()

import common,curl
mloger = common.myLogger(user='api_monitor', file_name='api_monitor.log')

def listen(event):
    if event.exception:
        global scheduler
        try:
            error_job_id = event.job_id
            if re.search(r'{}$'.format(SCHEDULER_JOB_ID['read']),
                         error_job_id):
                mloger.error('读取任务{}失败，错误原因：{}..................................'.format(
                    error_job_id, event.traceback
                ))
                scheduler.shutdown(wait=False)
            else:
                mloger.error('{}任务被丢弃，错误原因：{}.....................................'.format(
                    error_job_id,event.traceback ## event.traceback获取异常,event.exception.message获取简略信息
                ))
                scheduler.remove_job( error_job_id )
        except apscheduler.jobstores.base.JobLookupError:
            mloger.error(traceback.format_exc())

def customer(args_dict, db_dict={} ):

    mloger.debug('***************** 执行任务，id为{}'.format(args_dict['id']))
    method = 'get'
    for tu in args_dict['METHOD_CHOICE']:
        if args_dict['method'] == tu[0]:
            method = tu[1]
            break
    try:
        d = json.loads( args_dict['expect_oput'])
    except:
        d = {}
    err,ret,put = curl.my_pycurl( args_dict['url'], method=method,
                                  params=args_dict['params'],
                                  expt_ret=args_dict['expect_ret'],
                                  expt_put=d )
    with common.dbExec(db_dict) as cursor:
        R_SQL = """UPDATE monitorapi
                  SET error={err},oput=%s,
                  ret={ret},update_time='{up_time}'
                  WHERE id={id}"""
        try:
            if isinstance(put, bytes):
                put = put.decode('UTF-8')
            if isinstance(put, str):
                R_SQL = R_SQL.format(
                    err=err, ret=ret,
                    up_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    id=args_dict['id']
                )
                cursor.execute(R_SQL, [put])
            else:
                mloger.error( '{}返回HTML代码类型({})不匹配，无法更新数据'.format(args_dict['id'],type(put)))
        except Exception as e:
            mloger.error( e.args )



def _get_id(id):
    return str(id)

def producer():
    if MUTEX.acquire():
        with common.dbExec( DATABASES['default'] ) as db_conn:
            # 新增或修改的监控项执行启动
            SQL = 'SELECT id,method,expect_oput,expect_ret,url,params,cron FROM monitorapi WHERE stat IN %s'
            db_conn.execute(SQL,(( MonitorApi.START, MonitorApi.CHANGING),))
            args_list = db_conn.fetchall()
            for args in args_list:
                id, method, expect_oput, expect_ret, url, params, cron = args
                sid = _get_id( id )
                if scheduler.get_job(sid):
                    mloger.info("id" + sid + "job存在，直接将当前任务移除避免出现参数不匹配")
                    scheduler.remove_job(sid)
                try:
                    args_dict = {
                        'id': id,
                        'METHOD_CHOICE': MonitorApi.METHOD_CHOICE,
                        'method': method,
                        'expect_oput': expect_oput,
                        'expect_ret': expect_ret,
                        'url': url,
                        'params': params,
                    }
                    db_conn.execute( 'UPDATE monitorapi set stat=%s WHERE id=%s', (MonitorApi.RUNING,id) )
                    scheduler.add_job(
                        customer, args=(args_dict,DATABASES['default']),
                        trigger='cron', minute='*/{}'.format(int(cron)),  # 设定轮询时间
                        coalesce=True, misfire_grace_time=10,
                        replace_existing=True, max_instances=1,
                        id=sid
                    )
                    mloger.debug("新增任务成功，id为{}".format(sid))
                except Exception as e:
                    mloger.error("新增ID{}异常，异常信息为{}".format(sid, e.args))
            # 等待关闭的监控项移除
            db_conn.execute('SELECT id FROM monitorapi WHERE stat={}'.format(MonitorApi.STOPING))
            stoping_ids = db_conn.fetchall()
            for tu in stoping_ids:
                s_id = _get_id(tu[0])
                try:
                    scheduler.remove_job(s_id)
                    mloger.debug("删除任务成功，id为{}".format(M.id))
                except apscheduler.jobstores.base.JobLookupError:
                    mloger.error("id{}任务不存在".format(s_id))
                finally:
                    try:
                        stop_sql = 'UPDATE monitorapi set stat={} WHERE id={}'.format(MonitorApi.STOPED, s_id)
                        mloger.debug(stop_sql)
                        db_conn.execute(stop_sql)
                    except:
                        mloger.error('将id为{}的任务修改状态为关闭的时候出错了'.format(s_id))
        MUTEX.release()



def pod_monitor():
    from DevopsManager.conf import DATABASES
    mloger.debug('*****************执行pod监控任务*********************')
    db_host = DATABASES['default']['HOST']
    db_user = DATABASES['default']['USER']
    db_passwd = DATABASES['default']['PASSWORD']
    db_port = DATABASES['default']['PORT']
    db_db = DATABASES['default']['NAME']

    conn = pymysql.connect(host=db_host, port=int(db_port), user=db_user, passwd=db_passwd, db=db_db, charset='utf8')
    cursor = conn.cursor()
    nowtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    data = os.popen('kubectl top pod | grep -v NAME')
    print(nowtime)
    for i in data:
        li = i.split()
        _pod_name = li[0].split('-')
        pod_name = '-'.join(_pod_name[0:-3])
        pod_cpu = li[1]
        pod_mem = li[2]
        try:
            cursor.execute("insert into server_podmonitor (`name`, cpu, mem, `time`) value (%s, %s, %s, %s)",
                           (pod_name, pod_cpu, pod_mem, nowtime))

            conn.commit()
        except Exception:
            mloger.error("发生异常", Exception)
            break

    cursor.close()
    conn.close()
def main( q_number=300, **kwargs ):
    sqlite_f = 'jobs.sqlite'
    if os.path.isfile(sqlite_f):
        os.remove( sqlite_f )
    global scheduler
    jobstores = {
        'default': SQLAlchemyJobStore(url='sqlite:///{}'.format(sqlite_f))
    }

    executors = {
        'default': ThreadPoolExecutor( q_number ),
    }

    scheduler = BlockingScheduler(
        jobstores=jobstores,
        executors=executors,
        timezone=utc
    )
    scheduler.add_listener( listen, EVENT_JOB_EXECUTED | EVENT_JOB_ERROR)
    # 启动程序需要将状态为执行中的任务添加进来
    with common.dbExec(DATABASES['default']) as db_conn:
        SQL = 'SELECT id, method, expect_oput, expect_ret, url, params, cron FROM monitorapi WHERE stat = {stat}'.format(
            stat=MonitorApi.RUNING)
        mloger.info(SQL)
        db_conn.execute(SQL)
        args_list = db_conn.fetchall()
        for args in args_list:
            id, method, expect_oput, expect_ret, url, params, cron = args
            sid = _get_id(id)
            if scheduler.get_job(sid):
                mloger.debug("id" + sid + "job存在，直接将当前任务移除避免出现参数不匹配")
                scheduler.remove_job(sid)
            try:
                args_dict = {
                    'id': id,
                    'METHOD_CHOICE': MonitorApi.METHOD_CHOICE,
                    'method': method,
                    'expect_oput': expect_oput,
                    'expect_ret': expect_ret,
                    'url': url,
                    'params': params,
                }
                scheduler.add_job(
                    customer, args=(args_dict,DATABASES['default']),
                    trigger='cron', minute='*/{}'.format(int(cron)),  # 设定轮询时间
                    coalesce=True, misfire_grace_time=10,
                    replace_existing=True, max_instances=1,
                    id=sid
                )
                mloger.debug("第一次添加任务成功，id为{}".format(sid))
            except Exception as e:
                mloger.error("第一次添加ID{}异常，异常信息为{}".format(sid, e.args))
    # 生产者
    scheduler.add_job( producer, trigger='cron',
                       args=(),
                       second = '*/{}'.format(3),
                       coalesce = True,
                       misfire_grace_time = 30,
                       max_instances = 1 ,
                       id = '{}'.format(SCHEDULER_JOB_ID['read']),
                       **kwargs
                       )
    ## pod_monitor
    scheduler.add_job(pod_monitor, trigger='cron',
                      minute='*/{}'.format(5),
                      )
    mloger.info('添加更新轮询JOB的任务')
    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass



    scheduler.shutdown(wait=False)

if __name__ == "__main__" :
    current_path = os.path.dirname(
        os.path.split(os.path.realpath(__file__))[0])
    sys.path.append(current_path)
    os.environ['DJANGO_SETTINGS_MODULE'] = 'DevopsManager.settings'
    from django.core.wsgi import get_wsgi_application
    from DevopsManager.settings import DATABASES
    application = get_wsgi_application()
    from ManagerMaster.models import MonitorApi
    main()

