
import pycurl
from urllib.parse import urlencode
from io import BytesIO,StringIO
import re

import socket
import json
import certifi

E_RET = 1;E_OPUT = 2;E_ALL = 3;E_FAIL=4;R_ALL = 100
ERROR_CHOICE = (
        (E_FAIL,'请求接口失败'),
        (E_RET,'状态码不匹配'),
        (E_OPUT,'返回值不匹配'),
        (E_ALL,'状态码和返回值都不匹配'),
        (R_ALL,'匹配'),
    )


def my_pycurl( url,method='get',
               params='{}',expt_ret=200,expt_put={}):

    io = BytesIO()

    c = pycurl.Curl()

    c.setopt(c.HTTPHEADER, ["Content-Type:application/json"])

    method = method.upper()
    c.setopt(pycurl.CUSTOMREQUEST, method)


    headers = {}
    def header_function(header_line):
        try:
            header_line = header_line.decode('UTF-8')
        except:
            pass
        if ':' not in header_line:
            return
        name, value = header_line.split(':',1)
        name = name.strip()
        value = value.strip()
        name = name.lower()
        headers[name] = value

    c.setopt(pycurl.HEADERFUNCTION, header_function)

    c.setopt(pycurl.WRITEFUNCTION, io.write)
    c.setopt(pycurl.FOLLOWLOCATION, 1)


    c.setopt(pycurl.CAINFO, certifi.where())


    dp = json.loads(params)
    if dp:
        if method == "POST":
            c.setopt(c.POSTFIELDS, params )
        elif method == "GET":
            url = "{}?{}".format(url,urlencode(dp))

    c.setopt(c.URL, url)

    c.setopt(pycurl.USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)")


    c.setopt(pycurl.NOSIGNAL, 1) # 将signal全部关闭,否则的话,出现解析DNS超时后,会引起libcurl crash
    c.setopt(pycurl.MAXREDIRS,1) # 设置重定向次数
    c.setopt(pycurl.CONNECTTIMEOUT,10)
    c.setopt(pycurl.TIMEOUT,300)
    try:
        c.perform()
    except pycurl.error as error:
        errno, errstr = 700, str(error)
        return E_FAIL,errno,errstr
    except Exception as e:
        return E_FAIL,700,str(e.args)
    try:
        ret = c.getinfo(c.HTTP_CODE)
        error_ret = R_ALL
        error_put = R_ALL
        encoding = None
        if 'content-type' in headers:
            content_type = headers['content-type'].lower()
            match = re.search('charset=(\S+)', content_type)
            if match:
                encoding = match.group(1)
        if encoding is None:
            put = io.getvalue()
        else:
            put = io.getvalue().decode(encoding)
    except Exception as e:
        return E_FAIL,700,str(e.args)
    finally:
        io.close()
        c.close()
    if expt_ret != int(ret):
        error_ret = E_RET
    if expt_put:
        try:
            d = json.loads( put )
            if expt_put != d :
                error_put = E_OPUT
        except:
            print(" 没办法做json解析，数据为 {}".format( put))
            error_put = E_OPUT

    if error_ret != R_ALL and error_put != R_ALL:
        return E_ALL,ret,put
    elif error_ret == R_ALL and error_put != R_ALL:
        return error_put,ret,put
    elif error_ret != R_ALL and error_put == R_ALL:
        return error_ret,ret,put
    else:
        return R_ALL,ret,put


if __name__ == "__main__":
    url = 'http://sp.fingercrm.cn/zjqd-web/channels/sp/rebate/getRebateList.do'
    params = '{"publicId":"gh_155170e9f6b2","time":1517322482388,"accesstoken":"0bab9acafbc23ce0d4f9c59ec631fb4e","companyId":"0","startSize":0,"curPage":1,"pageSize":15,"rowsCount":0,"pageCount":0}'
    print( my_pycurl('https://www.baidu.com/'))
    url = 'http://sp.fingercrm.cn/zjqd-web/channels/sp/rebate/getRebateList.do'
    params='{"publicId":"gh_155170e9f6b2","time":1517322482388,"accesstoken":"0bab9acafbc23ce0d4f9c59ec631fb4e","companyId":"0","startSize":0,"curPage":1,"pageSize":15,"rowsCount":0,"pageCount":0}'
    print(  my_pycurl(url,params=params,method="post") )
    # print('#' *300)
    # print(a)
    # print(b)
    # print('*'*20)
    # print(c)
