#!/bin/bash
app_name=$1

kubectl rollout history deploy/${app_name}-deployment |awk -F':' '$1 ~ /[0-9]+/{split($1,b," ");split($2,a," ");if(a[1] ~ /[0-9]+/){print b[1]":"a[1]}}'
