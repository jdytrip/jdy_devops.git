#!/bin/bash
app_type="$1"
app_name="$2"
app_parm="$3"
git_addr="$4"
git_branch="$5"
packet_name="$6"
work_dir="/data/k8s"
packet_dir="${work_dir}/${app_name}/build_space/${packet_name}"
npm_dir=`dirname $packet_dir`
npm_name=`basename $packet_dir`

p_e(){
        echo -e "`date +%F' '%T`\e[031m [ERR] \e[0m$*" 2>&1
        exit 1
}
p_s(){
        echo -e "`date +%F' '%T`\e[032m [LOG] \e[0m$*" 2>&1
}

# 拉取代码
git_code(){
        test -d $work_dir || p_e "$work_dir 不存在!!!"
        cd $work_dir
        if [ -d $app_name ];then
                cd $app_name
        else
                mkdir -p $app_name && cd $app_name
        fi

        if [ -d build_space ];then
                cd build_space
                git checkout $git_branch && git pull
                if [ $? != 0 ];then
			cd ${work_dir}/${app_name} && rm -rf build_space && git clone $git_addr build_space
                	[ $? == 0 ] || p_e "git clone 失败！请联系运维处理！"
                	cd build_space
                	git checkout $git_branch && git pull
                	[ $? == 0 ] || p_e "git checkout 失败！请联系运维处理！"
		fi
        else
                git clone $git_addr build_space
                [ $? == 0 ] || p_e "git clone 失败！请联系运维处理！"
                cd build_space
                git checkout $git_branch && git pull
                [ $? == 0 ] || p_e "git checkout 失败！请联系运维处理！"
        fi
}


# maven 构建
mvn_build(){
	/usr/local/maven3/bin/mvn install $app_parm
        [ $? == 0 ] || p_e "mvn 构建失败！请联系开发处理！"
	

}

# node js 构建
npm_build(){
	/usr/local/bin/npm install --unsafe-perm --verbose
        [ $? == 0 ] || p_e "npm install 失败 请联系开发处理！"
        $app_parm
        [ $? == 0 ] || p_e "npm run build 失败！请联系开发处理！"
	test -d $npm_dir && cd $npm_dir || p_e "$npm_dir 不存在！请联系运维处理！"
        tar -cvf $npm_name *
	[ $? == 0 ] || p_e "打包$npm_name失败 请联系运维处理！"
}

if [ $app_type == "jar" ];then
	git_code
	mvn_build
elif [ $app_type == "war" ];then
	git_code
	mvn_build
elif [ $app_type == "node" ];then
	git_code
	npm_build
else
	p_e "app_type error !!!"
fi
