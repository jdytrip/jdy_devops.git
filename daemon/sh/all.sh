#!/bin/bash
MY_PATH=$(cd `dirname $0`;pwd)
cd $MY_PATH

app_type="$1"
app_name="$2"
app_parm="$3"
git_addr="$4"
git_branch="$5"
packet_name="$6"

p_e(){
        echo -e "`date +%F' '%T`\e[031m [ERR] \e[0m$*" 2>&1
        exit 1
}
p_s(){
        echo -e "`date +%F' '%T`\e[032m [LOG] \e[0m$*" 2>&1
}


del_app(){
        if [ -d /data/k8s/${app_name}/build_space ];then
                rm -rf /tmp/${app_name}/build_space && mv /data/git/${app_name}/build_space /tmp/${app_name}/
                [ $? == 0 ]&& p_s "true" || p_e "false"
        else
                p_s "true"
        fi
}


# 检查参数
if [ $# == 6 ];then
        p_s "参数正确"
	/bin/sh /root/sh/build.sh "$app_type" "$app_name" "$app_parm" "$git_addr" "$git_branch" "$packet_name"
	[ $? == 0 ] || p_e "构建失败，停止操作！"
	/bin/sh /root/sh/docker.sh "$app_type" "$app_name" "$packet_name"
	[ $? == 0 ] || p_e "发布失败，停止操作！"
elif [ $# == 2 -a $1 == "del_app" ];then
        del_app
        exit 0
elif [ $# == 2 -a $1 == "history" ];then
	sh /root/sh/history.sh $app_name
elif [ $# == 3 -a $1 == "rollout" ];then
	tag="$3"
	sh /root/sh/rollout.sh "$app_name" "$tag"
	exit 0
else
        p_e "缺少参数 eg: 发布类型 发布工程名 打包参数 git地址 git分支 包名 OR del_app 发布工程名 OR rollout 发布工程名 时间戳"
fi


