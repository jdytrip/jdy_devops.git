#!/bin/bash

install_java8(){
    wget -c -q  http://sh.17kantv.com/pkg/jdk-8u144-linux-x64.tar.gz || echo "下载jdk8失败"
    tar -xf jdk-8u144-linux-x64.tar.gz
    mv jdk1.8.0_144 /usr/local/java

}

install_mvn(){
    wget -c -q http://mirrors.hust.edu.cn/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz || echo "下载mvn包失败"
    tar -zxf apache-maven-3.5.3-bin.tar.gz
    mv apache-maven-3.5.3 /usr/local/maven3
}

install_npm(){
    wget -c -q http://nodejs.org/dist/v8.9.4/node-v8.9.4-linux-x64.tar.gz || echo "下载npm包失败"
    tar -zxf node-v8.9.4-linux-x64.tar.gz
    mv node-v8.9.4-linux-x64 /usr/local/node

}


export_path(){
    cat >> $HOME/.bash_profile <<EOF

#config
export JAVA_HOME=/usr/local/java
export JRE_HOME=\${JAVA_HOME}/jre
export CLASSPATH=.:\${JAVA_HOME}/lib:\${JRE_HOME}/lib
export M2_HOME=/usr/local/maven3
export NODE_HOME=/usr/local/node
export PATH=\${JAVA_HOME}/bin:\${M2_HOME}/bin:\${NODE_HOME}/bin:\$PATH

EOF
    source $HOME/.bash_profile
}

main(){
    install_java8
    install_mvn
    install_npm
    export_path
}


main