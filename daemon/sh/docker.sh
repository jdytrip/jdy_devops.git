#!/bin/bash
login_user="pengmd@fingercrm.cn"
login_passwd="Misadmin.321"
login_addr="registry-vpc.cn-hangzhou.aliyuncs.com"
app_type="$1"
app_name="$2"
packet_name="$3"
work_dir="/data/k8s"
packet_dir="${work_dir}/${app_name}/build_space/${packet_name}"
npm_dir=`dirname $packet_dir`
npm_name=`basename $packet_dir`
docker_addr="${work_dir}/${app_name}/Docker_space"
tag=`date +%s`
image_name="registry-vpc.cn-hangzhou.aliyuncs.com/docker_kubernetes/${app_name}:$tag"

p_e(){
        echo -e "`date +%F' '%T`\e[031m [ERR] \e[0m$*" 2>&1
        exit 1
}
p_s(){
        echo -e "`date +%F' '%T`\e[032m [LOG] \e[0m$*" 2>&1
}

docker_build(){
        test -f $packet_dir || p_e "包路径${packet_dir}未找到！ 请联系运维处理！"
        if [ $app_type == "jar" ];then
                test -d $docker_addr && rm -fr $docker_addr/*.jar || p_e "docker build 目录不存在！请联系运维处理！"
        elif [ $app_type == "war" ];then
                test -d $docker_addr && rm -fr $docker_addr/*.war || p_e "docker build 目录不存在！请联系运维处理！"
	elif [ $app_type == "node" ];then
		test -d $docker_addr && rm -fr $docker_addr/*.tar || p_e "docker build 目录不存在！请联系运维处理！"
        fi
	test -d $docker_addr || p_e "$docker_addr 不存在！！！请联系运维处理！"
	test -f $packet_dir || p_e "$packet_dir 不存在！！！请联系运维处理！"
        \cp $packet_dir $docker_addr
        cd $docker_addr
	# docker 构建镜像
        docker build -t $image_name . >>/dev/null
        #docker build -t $image_name . 
	[ $? == 0 ] || p_e "docker build 失败！请联系运维处理！"
	# 登录阿里云镜像仓库
        docker login -u $login_user -p $login_passwd $login_addr
        [ $? -eq 0 ] || p_e "登录docker仓库失败！请联系运维处理！"
	# 推送本地镜像到阿里云镜像仓库
        docker push $image_name
        [ $? == 0 ] || p_e "push docker镜像失败！请联系运维处理！"
	# 滚动发布
	kubectl set image deployment/${app_name}-deployment ${app_name}=${image_name} --record
	[ $? == 0 ] || p_e "kubectl 重新部署失败！！请联系运维处理！"
	for i in {1..100}
        do
        /usr/local/bin/kubectl get pod | grep $app_name | grep -v Running
        if [ $? != 0 ];then
                p_s "发布成功！"
                return 1
        fi
        echo "==============================================正在更新容器======================================================"
        sleep 2
        done
}

docker_build
[ $? != 0 ] || p_e "发布失败，请联系运维处理！"
/usr/local/bin/kubectl get pod | grep $app_name
