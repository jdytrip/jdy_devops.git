#!/bin/bash
app_name="$1"
tag="$2"

p_e(){
        echo -e "`date +%F' '%T`\e[031m [ERR] \e[0m$*" 2>&1
        exit 1
}
p_s(){
        echo -e "`date +%F' '%T`\e[032m [LOG] \e[0m$*" 2>&1
}
check_tag(){
	/usr/local/bin/kubectl rollout history deployment ${app_name}-deployment | grep $tag >>/dev/null 
}

rollout(){
	revision=$(/usr/local/bin/kubectl rollout history deployment ${app_name}-deployment | grep $tag | awk '{print $1}')
	/usr/local/bin/kubectl rollout undo deployment/${app_name}-deployment --to-revision=$revision
	[ $? == 0 ] || p_e "回滚失败，请联系运维处理！！"
	for i in {1..100}
	do
	/usr/local/bin/kubectl get pod | grep $app_name | grep -v Running
	if [ $? != 0 ];then
		p_s "回滚成功！"
		return 1
	fi
	echo "==============================================正在更新容器======================================================"
	sleep 2
	done
}


if [ $# == 2 ];then
	check_tag || p_e "查询版本失败，请确认版本！！"
	rollout
	[ $? != 0 ] || p_e "回滚失败，请联系运维处理！"
	/usr/local/bin/kubectl get pod | grep $app_name
else
	p_e "参数错误，请联系运维处理！！" 
fi
