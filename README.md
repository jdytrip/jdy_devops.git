# Yokiy 运维自动化平台项目
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/qq群.jpg)
- 项目地址：[https://gitee.com/jdytrip/jdy_devops.git](https://gitee.com/jdytrip/jdy_devops.git)
- DEMO: [http://demo.yokiy.com/](http://demo.yokiy.com) 用户名 admin 密码 admin123
---

## 项目简介
- 这是一个web自动化平台，为企业实现一键部署。
- 基于Python 3 和 Django2.0
- Bootstrap ampleadmin 模板
- Docker kubernetes
- Mysql 5.6 & Redis 4.0


## 主要功能
1. 用户权限管理
1. CI/CD 持续部署，持续构建
1. MySql执行管理
1. 应用日志管理
1. 应用监控
1. API监控

## TODO
1. 自动创建应用，yml配置文件前置
1. 监控告警通知
1. 配置模板
1. 多构建节点
1. 发布审核
1. API监控多节点
1. 文档管理
1. 短信验证
1. 钉钉加入

## 依赖
- django==2.0
- requests==2.18.4
- mysqlclient==1.3.12
- PyMySQL==0.8.0
- pycurl==7.43.0.1
- APScheduler==3.0.3
- SQLAlchemy==1.1.7
- celery==4.1.0
- django-celery-results==1.0.1
- django-redis==4.8.0

## 效果图
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/1.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/2.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/3.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/4.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/5.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/6.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/7.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/8.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/9.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/10.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/11.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/12.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/13.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/14.png)
 ![gitee](https://gitee.com/jdytrip/jdy_devops/raw/master/static/screenshot/15.png)



## 部署步骤
### 安装版
- 部署kubernetes并[安装kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- 安装mysql & redis & nginx
- 安装pyenv：
```python
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash    # 安装虚拟环境，脚本跑完之后会有提示 按照提示修改自己的`~/.bash_profile
# 打开配置文件
vim ~/.bash_profile

# 复制刚才提示中的类似这三段
# 不能直接复制我的,每个人的不一样
export PATH="/root/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-"

# 使自己的profile生效
source ~/.bash_profile
```

- 安装Python3:
```python
pyenv install 3.6.5
```

- 创建运维平台运行的虚拟环境
```python
pyenv virtualenv 3.6.5 venv
```

- Clone并编写配置:
```python
git clone -b master https://gitee.com/jdytrip/jdy_devops.git
cd /data/jdy_devops  # 进入网站目录
pyenv local venv # 将项目文件夹的环境设置为 virtualenv
```


- 修改jdyops/conf.py 配置文件
- 安装依赖：
```python
pip install -r requirements.txt
```
- 初始化数据库：
```python
python manage.py makemigrations django_celery_results  # 可忽略
python manage.py makemigrations server
python manage.py migrate
```
- 初始化管理员数据和权限数据:
```python
python ./daemon/init_db.py
```
>注意，管理员账号初始化为admin，密码为admin123

- 后台程序启动:
```python
celery multi start  -A jdyops worker -l info -c 8 -n worker1.%h  # 启动多进程 -c {进程数}
```
- 启动定时任务程序:
```python
nohup python ./daemon/api_monitor.py >>./logs/api_monitor.log 2>&1 &
```

- 启动nginx: 将daemon/jdy_devops.cnf 拷贝到nginx/conf/vhost下，并修改配件文件相应字段，然后启动

- 启动uwsgi
```python
uwsgi uwsgi.ini
```

- 安装构建环境
  - 生成openssh key，将key和pub放入 daemon/ssh_key/ 目录下
  - 安装JDK8，MAVEN3，NODEJS
```python
# 到构建节点机器上运行，前期可以与web在同一台机器
sh jdy_devops/daemon/sh/install_mvn_npm.sh
```
- 前台启动celery
```python
celery -A ManagerMaster worker --pool=solo -l info
```







### Docker版
- 安装docker：
  - 根据自己的系统
  - 安装docker 和 docker-compose
  - 教程: https://docs.docker.com/install/

- 下载源码：
  - https://gitee.com/jdytrip/jdy_devops.git

- 修改配置文件DevopsManager/conf.py
- 修改jdyops/nginx/nginx.example.conf

```python
server
    {
     	listen 80;
        server_name _;  # 这个地方填写你的域名
        root  /src/django-sspanel;

	location /static
        {
	alias  /src/django-sspanel/static; #静态文件地址，js/css
        expires  12h;
        }

	location /
        {
	include uwsgi_params;
        uwsgi_pass web:8080;
        }

        location = /favicon.ico {
        empty_gif;
        }

    }
```

- 运行程序
```python
# 进入项目根目录
cd jdy_devops

# 开启程序
docker-compose up -d
```


- 初始化数据库
```python
docker exec -it devops-web /usr/local/bin/python /src/yokiy-devops/manage.py makemigrations server
docker exec -it devops-web /usr/local/bin/python /src/yokiy-devops/manage.py migrate
docker exec -it devops-web /usr/local/bin/python /src/yokiy-devops/daemon/init_db.py
```

- 安装构建环境
  - 生成openssh key，将key和pub放入 daemon/ssh_key/ 目录下
  - 安装JDK8，MAVEN3，NODEJS
```python
# 到构建节点机器上运行，前期可以与web在同一台机器
sh jdy_devops/daemon/sh/install_mvn_npm.sh
```


开始访问你的域名吧~

