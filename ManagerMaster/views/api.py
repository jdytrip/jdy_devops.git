from django.shortcuts import render
from django.urls import reverse
from django.template import Context, Template
import re
from ..models import *
from django.http import HttpResponse, HttpResponseRedirect
RANDOM_NUMBER = 199999090122

def api_index(request):
    """API监控首页
    /server/project/<int:project_id>/api/index/
    """
    comm_re = re.compile(reverse('ManagerMaster:api_detail', args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    pro_list = []
    url_list = request.session['authority']
    for url in url_list:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if ProjectModel.objects.filter(pk=pid).exists():
                pro_list.append(pid)
    if pro_list:
        pro_list = list(set(pro_list))
        return HttpResponseRedirect(reverse('ManagerMaster:api_detail',
                                            args=(pro_list[0],)))
    error = {
        'title': '无法找到相关项目',
        'code': 404,
        'msg': "无法找到相关的项目"
    }
    return render(request, 'error.html', {'error': error, 'active': 'api_admin'})


def api_detail(request,project_id):
    '''API监控详情页'''
    try:
        project_node = ProjectModel.objects.get(pk=project_id)
    except:
        error = {
            'title' : '找不到项目',
            'code' : 404,
            'msg' : '此监控项目无法找到'
        }
        return render(request,'error.html',{'error':error})
    ret_d = {}
    pro_list = []
    project_list = []
    comm_re = re.compile(reverse('ManagerMaster:api_detail', args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    for url in request.session['authority']:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if ProjectModel.objects.filter(pk=pid).exists():
                pro_list.append(pid)
    if pro_list:
        # 排序，确保获取到的项目按照主键升序
        for p_id in list(set(pro_list)):
            if p_id != project_id:
                project_list.append(ProjectModel.objects.get(pk=p_id))
    if request.method == "POST":
        # 新建或修改API监控数据
        form = None
        if 'edit_id' in request.POST:
            edit_id = request.POST.get('edit_id')
            try:
                node = MonitorApi.objects.get(pk=int(edit_id))
                form = MonitorApiForm(request.POST,instance=node)
            except:
                pass
        if not form:
            form = MonitorApiForm(request.POST)
        if form.is_valid():
            auth = True
            try:
                json.loads(form.cleaned_data['params'])
            except:
                form.add_error('params','该字段必须为json格式')
                auth = False
            try:
                json.loads(form.cleaned_data['expect_oput'])
            except:
                form.add_error('expect_oput','该字段必须为json格式')
                auth = False
            if auth:
                api_node = form.save(commit=False)
                api_node.project = project_node
                try:
                    api_node.save()
                    try:
                        MonitorApi.objects.filter(pk=int(edit_id)).update(stat=MonitorApi.CHANGING)
                    except:
                        pass
                    d = {'type': 'success', 'msg': '新建监控项成功'}
                except Exception as e:
                    d = {'type': 'error', 'msg': '{}'.format(e.args)}
                finally:
                    return HttpResponseRedirect("{}?msg={}".format( reverse('ManagerMaster:api_detail',args=(project_id,)),
                                                                    json.dumps(d, ensure_ascii=False)
                                                                    ))
            else:
                return render(request, 'api_edit.html', {'form': form, 'project_instance': project_node})

        else:
            return render(request, 'api_edit.html', {'form':form,
                                                     'project_instance': project_node})
    try:
        # 更新监控api状态
        ids = request.GET.get("ids")
        ids_list = []
        t = Template("{{oput| escape| linebreaks}}")
        tt = Template("{{update_time|date:'Y-m-d H:i:s'}}")
        for id in ids.strip().strip(",").split(','):
            id = int(id)
            try:
                temp_node = MonitorApi.objects.get(pk=id)
                temp_d = {
                    'id': id,
                    'error' : temp_node.error,
                    'ret' : temp_node.ret,
                    'oput': t.render(Context({'oput':temp_node.oput})),
                    'time': tt.render(Context({'update_time':temp_node.update_time})),
                }
            except Exception as e:
                temp_d = {
                    'id': id,
                    'error':110,
                    'ret': 0,
                    'oput': {},
                    'time' : 0,
                }
            finally:
                ids_list.append(temp_d)
        return HttpResponse(json.dumps(ids_list))
    except :
        pass
    try:
        a = request.GET.get('msg')
        ret_d = dict(json.loads(a),**ret_d)
    except:
        pass

    monitor_list = MonitorApi.objects.filter(
        project= project_node).exclude(stat__in=(MonitorApi.STOPING,MonitorApi.STOPED))
    return render(request, 'api_detail.html', dict({
        'monitor_list' : monitor_list,'project_node' : project_node,
        'project_list' : project_list
    }, **ret_d))

def api_edit(request,project_id):
    ''' API监控新增和修改'''
    project_node = ProjectModel.objects.get(pk=project_id)
    if request.method == "GET" and 'edit_id' in request.GET:
        node_id = request.GET.get('edit_id')
        try:
            node = MonitorApi.objects.get(pk=node_id)
            form = MonitorApiForm(instance=node)
            return render(request, 'api_edit.html', {'form': form, 'edit_id': node_id,
                                                     'api_name': node.name+' API监控修改',
                                                     'project_node': project_node})
        except Exception as e:
            form = MonitorApiForm()
    else:
        form = MonitorApiForm()
    return render(request, 'api_edit.html', {'form': form,
                                             'api_name': project_node.project_name+' API监控新增',
                                             'project_node':project_node})

def api_delete(request,project_id):
    '''api监控删除'''
    project_node = ProjectModel.objects.get(pk=project_id)
    if request.method == "GET" and 'del_id' in request.GET:
        node_id = request.GET.get('del_id')
        try:
            MonitorApi.objects.filter(pk=node_id).update(stat=MonitorApi.STOPING)
        except:
            pass
    return HttpResponseRedirect(reverse('ManagerMaster:api_detail',args=(project_id,)))