from django.shortcuts import render
from DevopsManager import settings
from django.urls import reverse
from django.http import StreamingHttpResponse
from django.template import Context, Template
import os
import re
import time
from ..models import *
from django.http import HttpResponse, HttpResponseRedirect
from ManagerMaster.views import tasks

RANDOM_NUMBER = 199999090122
def app_detail(request,project_id,app_id):
    '''APP应用查看'''
    app_instance = AppModel.objects.get(pk=app_id)
    # rollback_list = []
    # from .daemon_ops import get_rollback_ids
    # msg = get_rollback_ids(app_instance.app_name)
    # if msg:
    #     comm_re = re.compile(r'^(?:|\s+)([0-9]+):([0-9]+)(?:|\s+)$')
    #     for line in msg.splitlines():
    #         try:
    #             gt = comm_re.search(line)
    #             rollback_list.append({
    #                 'id': gt.group(1),
    #                 'name' : gt.group(2)
    #             })
    #         except:
    #             pass
    task_list =  TaskModel.objects.filter(
        task_app=app_instance).order_by('-id')[:10]
    return render(request, 'app_detail.html', {'app_instance': app_instance,
                                               # 'rollback_list' : rollback_list,
                                               'task_list':task_list,
                                               'project_id':project_id})

def app_edit(request, project_id, app_id):
    '''应用APP修改'''
    app_instance = AppModel.objects.get(pk=app_id)
    project_instance = app_instance.app_project
    if request.method == "POST":
        form = AppForm(request.POST, instance=app_instance)
        if form.is_valid():
            try:
                AppModel.objects.filter(pk=app_id).update(
                    app_pakage = form.cleaned_data['app_pakage'],
                    app_git=form.cleaned_data['app_git'],
                    app_branch=form.cleaned_data['app_branch'],
                    app_build=form.cleaned_data['app_build']
                )
                return HttpResponseRedirect(reverse('ManagerMaster:app_detail',args=(project_id,app_id)))
            except Exception as ex:
                if 'UNIQUE' in ','.join(str(i) for i in ex.args) :
                    form.add_error('app_name', "存在重复数据:{}".format(str(ex.args)))
                else:
                    form.add_error('app_name', str(ex.args))
    else:
        form = AppForm(instance=app_instance)
    return render(request, 'app_add.html',{'form': form, "display":app_instance.app_name+"修改参数",
                                           'project_instance': project_instance,'app_id':app_id})

def app_add(request, project_id):
    """APP新增
    权限表需要添加响应的权限记录
    """
    project_instance = ProjectModel.objects.get(pk=project_id)
    if request.method == "POST":
        form = AppForm(request.POST)
        if form.is_valid():
            app_instance = form.save(commit=False)
            app_instance.app_project = project_instance
            ret_url = reverse('ManagerMaster:app_list',args=(project_id,))
            try:
                app_instance.save()
                try:
                    # 添加权限表相关策略
                    app_node = AppModel.objects.get(app_name=form.cleaned_data["app_name"],
                                                    app_project=project_instance)
                    proj_parent = GlobalAuthority.objects.get(type=GlobalAuthority.PROJECT,
                                                              url=settings.PROJECT_ID_URL.format(project_id=project_id),
                                                              link_project=project_instance)
                    app_url =settings.PROJECT_ID_APP_ID_URL.format(
                        project_id=project_id,app_id=app_node.id)
                    app_parent = GlobalAuthority.objects.create(name=app_node.app_name,url=app_url,
                                                                parent=proj_parent,type=GlobalAuthority.APP,
                                                                link_app=app_node)
                    app_s_list = []
                    for act in [ {'name':'删除','url':'delete'},{'name':'增改','url':'edit'},{'name':'执行','url':'task'},{'name':'预览','url':'detail'},{'name':'日志','url':'log'}]:
                        app_s_list.append(
                            GlobalAuthority(name=act['name'],url=app_url+act['url']+'/',
                                            parent=app_parent,type=GlobalAuthority.APP,
                                            link_app=app_node)
                        )
                    GlobalAuthority.objects.bulk_create( app_s_list )
                    myself = request.user
                    add_auths = []
                    for j in GlobalAuthority.objects.filter(type=GlobalAuthority.APP,
                                                            link_app=app_node):
                        add_auths.append(UserAuthority(user=myself,authority=j))
                    UserAuthority.objects.bulk_create( add_auths)
                    # 优先刷新当前创建者权限缓存，避免跳转之后无法查到当前APP记录
                    tasks.update_cache_authority.delay([request.user.id])
                    # 更新权限数据库记录
                    tasks.update_db_authority.delay()
                    # 根据权限表数据刷新缓存中权限数据
                    tasks.update_cache_authority.delay()
                except Exception as ex:
                    err_msg = json.dumps({'type':'error','msg':','.join(str(i) for i in ex.args)})
                    return HttpResponseRedirect('{}?msg={}'.format(ret_url, err_msg))
                return HttpResponseRedirect('{}?msg={}'.format(ret_url,
                                                               json.dumps({'type':'success','msg':'创建APP成功'})))
            except Exception as ex:
                if 'UNIQUE' in ','.join(str(i) for i in ex.args):
                    form.add_error('app_name', "存在重复数据:{}".format(str(ex.args)))
                else:
                    form.add_error('app_name', str(ex.args))
    else:
        form = AppForm()
    return render(request, 'app_add.html', {'form': form, 'project_instance': project_instance,
                                            "display": project_instance.project_name+"新增APP配置"})

def app_delete(request, project_id, app_id):
    '''APP删除'''
    try:
        node = AppModel.objects.get(pk=app_id)
        # 执行远程脚本，清理build机器上目录
        tasks.clean_app_path.delay(node.app_name)
        node.delete()
        # 根据权限表数据刷新缓存中所有用户的权限记录
        tasks.update_cache_authority.delay()
    except:
        pass
    return HttpResponseRedirect(reverse('ManagerMaster:app_list',args=(project_id,)))


def app_index(request):
    """
    /ManagerMaster/project/<int:project_id>/app/list/
    APP首页
    """
    comm_re = re.compile(reverse('ManagerMaster:app_list',args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER),'([0-9]+)'))
    pro_list = []
    url_list = request.session['authority']
    for url in url_list:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if ProjectModel.objects.filter(pk=pid).exists():
                pro_list.append(pid)
    if pro_list:
        pro_list = list(set(pro_list))
        return HttpResponseRedirect(reverse('ManagerMaster:app_list',
                                            args=(pro_list[0],)))
    error = {
        'title': '无法找到相关项目',
        'code': 404,
        'msg': "无法找到相关的项目"
    }
    return render(request, 'error.html', {'error': error,
                                          'active': 'project_admin'})

def app_list(request,project_id):
    """显示指定项目组下所有的APP"""
    project_node = ProjectModel.objects.get(pk=project_id)
    user = request.user
    app_list =  []
    project_list = []
    comm_re = re.compile(reverse('ManagerMaster:app_list', args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    comm_re_one = re.compile(reverse('ManagerMaster:app_detail',args=(RANDOM_NUMBER,RANDOM_NUMBER)
                                     ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    for url in request.session['authority']:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if project_id == pid:
                continue
            try:
                project_list.append(
                    ProjectModel.objects.get(pk=pid)
                )
            except:
                pass
            continue
        try:
            a = comm_re_one.search( url )
            pid, aid = a.group(1),a.group(2)
            if int(pid) == project_id:
                app_list.append(AppModel.objects.get(pk=int(aid)))
        except:
            pass
    ## 查询监控数据
    data_monitor = PodMonitor.objects.raw('select * from ManagerMaster_podmonitor where time = (select max(time) from ManagerMaster_podmonitor)')
    pod_monitor = []
    for data in data_monitor:
        _data = {}
        _data['name'] = data.name
        _data['cpu'] = data.cpu
        _data['mem'] = data.mem
        pod_monitor.append(_data)

    return_dict = {'project_node': project_node,
                   'app_list': app_list,
                   'project_list': project_list,
                   'pod_monitor': pod_monitor}
    try:
        d = json.loads(request.GET.get('msg'))
        return_dict = dict(d, **return_dict)
    except:
        pass
    return render(request,'app_list.html',return_dict)

def task_add(request, project_id, app_id):
    '''新增task任务'''
    if request.method == 'GET' and 'rollback_id' in request.GET:
        rollback_id = int(request.GET.get('rollback_id',0))
    else:
        rollback_id = 0
    try:
        app_instance = AppModel.objects.get(pk=app_id)
        if app_instance.app_status == 100:
            task_instance = TaskModel.objects.create(
                task_app=app_instance,
                task_person=request.user.username
            )
            app_instance.app_status = AppModel.WAIT
            app_instance.save()
            if rollback_id:
                tasks.task_do.delay(task_instance.id, rollback_id)
            else:
                tasks.task_do.delay(task_instance.id)
        else:
            return HttpResponseRedirect(reverse('ManagerMaster:app_detail',args=(app_id,)))
    except Exception as exc:
        return HttpResponseRedirect('{}?msg={}'.format(reverse('ManagerMaster:app_detail',args=(project_id,app_id)),{'type':'error','data':str(exc.args)}))
    if rollback_id:
        print(json.dumps({'task_id':task_instance.id}))
        return HttpResponse(json.dumps({'task_id':task_instance.id})+'12')
    return HttpResponseRedirect('{}?task_id={}'.format(reverse('ManagerMasterr:task_log',args=(project_id,app_id)),task_instance.id))


def task_rollback(request, project_id, app_id):
    '''查询回滚版本'''
    app_name = AppModel.objects.get(pk=app_id).app_name
    from ManagerMaster.views.daemon_ops import get_rollback_ids
    msg = get_rollback_ids(app_name)
    if not msg:
        return HttpResponse()
    gs=[]
    comm_re = re.compile(r'^(?:|\s+)([0-9]+):([0-9]+)(?:|\s+)$')
    for line in msg.splitlines():
        try:
            gt = comm_re.search(line)
            ti = int(gt.group(2))
            gs.append({'id':gt.group(1),
                       'ro_id' : ti,
                       'name':time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(ti))
                       })
        except:
            pass
    from django.template.loader import get_template
    t = get_template('rollback_list.html')
    return HttpResponse(t.render({'rollback_list':gs,
                                  'project_id':project_id,'app_id':app_id,}))


def file_iterator(file_name, chunk_size=512):
    GG_T = Template("{{ log| escape| linebreaks}}")
    with open(file_name) as f:
        while True:
            c = f.read(chunk_size)
            if c:
                yield GG_T.render(Context({'log':c}))
            else:
                break

def task_log(request,project_id, app_id):
    '''task log'''
    app_instance = AppModel.objects.get(pk=app_id)
    if request.method != "GET":
        return render(request, '500.html')
    if "app_status" in request.GET: #   获取当前app状态
        return HttpResponse(
            json.dumps({
                'id': app_instance.id,
                'data': app_instance.app_status
            })
        )
    elif "task_id" in request.GET and "query_log" in request.GET: # 获取指定id日志
        try:
            task_id = request.GET.get('task_id')
            t = Template("{{task_log| escape| linebreaks}}")
            # log_file = os.path.join(
            #     settings.DAEMON_LOG_PATH,
            #     os.path.basename(TaskModel.objects.get(pk=task_id).task_log)
            # )
            f_file_name = '_{task_id}_{app_id}_{app_name}.log'.format(
                task_id=task_id, app_id=app_id,
                app_name=app_instance.app_name)
            log_file = os.path.join(
                settings.DAEMON_LOG_PATH, f_file_name
            )
            if not os.path.isfile( log_file ):
                return HttpResponse("无法找到日志文件")
            return StreamingHttpResponse(file_iterator(log_file))
            # with open( log_file) as f:
            #     d = {'task_log': f.read()}
            #     return HttpResponse(t.render(Context(d)))
        except Exception as exc:
            return HttpResponse( str(exc.args))

    elif "task_id" in request.GET: #  获取指定task实时日志，读取日志文件
        try:
            task_id = request.GET.get('task_id')
            task_instance = TaskModel.objects.get(id=task_id)
            error = ""
        except Exception as exc:
            error = exc.args
        if request.GET.get("just_json") and request.GET["just_json"]:

            # try:
            #     log = os.path.join(settings.DAEMON_LOG_PATH,
            #                        '_{task_id}_{app_id}_{app_name}.log'.format(
            #                            task_id=task_instance.id,
            #                            app_id=app_id,
            #                            app_name=app_instance.app_name
            #                        ))
            #     with open(log) as f:
            #         log_content = f.read()
            # except Exception as exc:
            #     import re
            #     error_str = ','.join(str(i) for i in exc.args)
            #     if re.search(r'No\s+such\s+file\s+or\s+directory', error_str):
            #         log_content = "等待任务开始执行"
            #     else:
            #         log_content = error_str
            # finally:
            #     t = Template("{{log_content| escape| linebreaks}}")
            #     return HttpResponse(t.render(Context( {'log_content': log_content} )))
            log = os.path.join(settings.DAEMON_LOG_PATH,
                               '_{task_id}_{app_id}_{app_name}.log'.format(
                                   task_id=task_instance.id,
                                   app_id=app_id,
                                   app_name=app_instance.app_name
                               ))
            if os.path.isfile( log ):
                return StreamingHttpResponse(file_iterator(log))
            else:
                log_content = "等待任务开始执行"
        else:
            return render(request, 'task_log.html',
                          {'error': error, 'app_instance': app_instance,
                           'project_id':project_id,'task_id': task_id})