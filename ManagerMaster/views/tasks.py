from __future__ import absolute_import
from celery import shared_task
from django.core.cache import cache
import re
from django.urls import reverse

from ManagerMaster.models import UserAuthority, GlobalAuthority, User



def gen_auth_list( id ):
    '''获取权限列表中URL列表'''
    lis = []
    pid_look = []
    is_api_look = False
    log_look = []
    key = 131499014
    comm_re_pid = re.compile(
        reverse('ManagerMaster:app_detail', args=(key,key)).replace(str(key), '([0-9]+)')
    )
    comm_re_api = re.compile(
        reverse('ManagerMaster:api_detail', args=(key,)).replace(str(key), '([0-9]+)')
    )
    comm_re_log = re.compile(
        reverse('ManagerMaster:app_log',args=(key,key)).replace(str(key), '([0-9]+)')
    )
    comm_re_app_add = re.compile(
        reverse('ManagerMaster:app_add',args=(key,)).replace(str(key), '([0-9]+)')
    )
    urls = UserAuthority.objects.filter(user_id=id).values_list('authority__url')
    for u in urls:
        url = u[0]
        if not url:
            continue
        lis.append( url )
        # 拥有查看项目应用权限
        try:
            pid = int(comm_re_pid.search(url).group(1))
            if pid not in pid_look:
                pid_look.append(pid)
            continue
        except:
            pass
        # 用户项目新增app权限则强制赋予查看权限
        try:
            pid=int(comm_re_app_add.search(url).group(1))
            if pid not in pid_look:
                pid_look.append(pid)
            continue
        except:
            pass

        # 拥有查看api监控列表权限
        try:
            pid = int(comm_re_log.search(url).group(1))
            if pid not in log_look:
                log_look.append(pid)
            continue
        except:
            pass
        # 拥有查看app日志权限
        if not is_api_look:
            if comm_re_api.search(url):
                lis.append(reverse('ManagerMaster:api_index'))
                is_api_look = True
    if pid_look:
        pid_look = list( set(pid_look))
        lis.append(reverse('ManagerMaster:app_index'))
        for i in pid_look:
            lis.append(reverse('ManagerMaster:app_list',args=(i,)))
    if log_look:
        log_look = list( set(log_look))
        lis.append(reverse('ManagerMaster:log_index'))
        for j in log_look:
            lis.append(reverse("ManagerMaster:log_list",args=(j,)))
    return lis




@shared_task
def update_cache_authority( ids = [] ):
    """获取用户权限表数据，刷新缓存中数据"""
    # from celery.utils.log import get_task_logger
    # logger = get_task_logger('x')
    ret = []
    if ids:
        for id in ids:
            key = 'user_{}'.format(id)
            value = gen_auth_list(id)
            cache.set(key, value, timeout=None)
            # cache.persist( key )
            ret.append({key:value})
        return ret
    ids = [ i['id'] for i in User.objects.values('id') ]
    for id in ids:
        key = 'user_{}'.format(id)
        try:
            if cache.get(key):
                value = gen_auth_list(int(id))
                cache.set(key, value, timeout=None )
                ret.append({key:value})
        except Exception as e:
            ret.append({key:str(e.args)})
    return ret


class MyTreeNode():
    '''遍历权限树'''
    def __init__(self, data={}):
        self.data = data
        self.checked = False
        self.children = []
        self.parent_node = None

    def to_dict(self):
        d = {"id": self.data['id']}
        if self.children:
            d['nodes'] = [child_node.to_dict() for child_node in self.children]
        return d

    @classmethod
    def get_tree_data(cls, user_obj):
        tree_data = []
        for obj in GlobalAuthority.objects.filter(parent=None):
            top_node = cls.gen_node(None, obj, user_obj)
            tree_data.append(top_node)
        dict_data = [td.to_dict() for td in tree_data]
        return dict_data

    @classmethod
    def gen_node(cls, parent_node, current_obj, user_obj):
        d = {"id": current_obj.id}
        node = MyTreeNode(d)
        children = GlobalAuthority.objects.filter(parent=current_obj)
        node.parent_node = parent_node

        check_objs = UserAuthority.objects.filter(user=user_obj,
                                                  authority=current_obj)

        if parent_node:
            if parent_node.checked == True:
                node.checked = True  # 如果父节点被选中，则子节点也被选中
                if not check_objs:  # 没有添加权限，则新增条目
                    UserAuthority.objects.create(user=user_obj,
                                                 authority=current_obj)
            else:
                if check_objs:  # 检查自己是否被选中
                    node.checked = True
            parent_node.children.append(node)
        else:
            if check_objs:
                node.checked = True
        if not children:
            return node
        else:
            for child in children:
                cls.gen_node(node, child, user_obj)
        return node


@shared_task
def update_db_authority(users=[]):
    """
    根据权限表父子规则，重新刷新用户权限表
    """
    if not users:
        users = User.objects.all()
    for user in users:
        MyTreeNode.get_tree_data( user )
    return

@shared_task
def task_do( task_id,rollback_id=None ):
    from ManagerMaster.views.daemon_ops import main
    return main( task_id,rollback_id )


@shared_task
def clean_app_path( app_name ):
    from ManagerMaster.views.daemon_ops import clean_app_path
    return clean_app_path( app_name )


@shared_task
def send_mail(subject, msg, to=[],from_email='',
              is_html = False,
              fail_silently=False):
    from django.core.mail import send_mail
    if not from_email:
        from DevopsManager import settings
        from_email = settings.EMAIL_HOST_USER
    if is_html:
        return send_mail( subject=subject,message="",from_email=from_email,recipient_list=to,
                          fail_silently=fail_silently,
                          html_message=msg)
    return send_mail( subject=subject,from_email=from_email,recipient_list=to,
                      message=msg,
                      fail_silently=fail_silently)


@shared_task
def exc_sql(sql_id, db_config, sql_file):
    from ManagerMaster.views.daemon_ops import remote_exc_sql
    ret = remote_exc_sql(sql_id, db_config, sql_file)
    if ret:
        return False
    return True

