from os.path import join as os_path_join
from time import sleep

from ManagerMaster.models import TaskModel,SqlNode
from . import common
from DevopsManager.settings import DAEMON_PATH
from DevopsManager.conf import BUILD_SERVER_IP, DAEMON_LOG_PATH


REMOTE_ACTION = "/usr/bin/ssh -i {ssh_key} -A -o StrictHostKeyChecking=no " \
          "-o GSSAPIAuthentication=no -o ConnectTimeout=10 " \
          "root@{build_server_ip}".format(ssh_key=os_path_join(DAEMON_PATH, 'ssh_key', 'id_rsa'),
                                          build_server_ip=BUILD_SERVER_IP)

def get_rollback_ids(app_name):
    cmd = "{act} 'sh /root/sh/all.sh history {app_name}'".format(
        act=REMOTE_ACTION,
        app_name=app_name
    )
    ret, out = common.sub_call(cmd)
    if ret:
        return '''30:1522170736
31:1522221875
32:1522227749
35:1522347483'''
    return out.strip()

def clean_app_path( app_name ):
    cmd = "{act} 'sh /root/sh/all.sh del_app \"{app_name}\" '".format(
        act = REMOTE_ACTION,
        app_name= app_name
    )
    ret, out = common.sub_call(cmd)
    if ret:
        return "删除{}目录失败".format(app_name) + out
    return "删除{}目录成功".format(app_name) + out


def main( task_id, rollback_id=None ):
    try:
        task_node = TaskModel.objects.get(pk=int(task_id))
    except Exception as e:
        return 'TASK_{}'.format(task_id),'查找任务id{}失败'.format(task_id),str( e.args)
    try:
        app_node = task_node.task_app
        app_d = {
            'id': app_node.id,
            'app_name': app_node.app_name,
            'app_pakage': app_node.app_pakage,
            'app_git': app_node.app_git,
            'app_branch': app_node.app_branch,
            'app_build': app_node.app_build,
        }
        if app_node.app_env == app_node.NODEJS:
            app_d['env_str'] = 'node'
        elif app_node.app_env == app_node.JAR:
            app_d['env_str'] = 'jar'
        elif app_node.app_env == app_node.WAR:
            app_d['env_str'] = 'war'
        app_node.app_status = app_node.ING
        app_node.save()
        task_node.task_status = task_node.ING
        task_node.save()
    except Exception as e:
        return 'TASK_{}'.format(task_id), '修改TASK和APP数据异常', str(e.args)
    return Consumer( task_id, app_d, rollback_id )

def Consumer( task_id, app, rollback_id=None ):
    try:
        app_id = app['id']
        f_file_name ='_{task_id}_{app_id}_{app_name}.log'.format(
                                    task_id=task_id, app_id=app_id,
                                    app_name=app['app_name'])
        log_file = os_path_join(DAEMON_LOG_PATH, f_file_name)
        spec_name = "{}.{}".format( app_id, task_id )
        logger = common.myLogger(user=spec_name, file_name=log_file)
    except Exception as e:
        return 'TASK_{}'.format(task_id),'生产日志对象异常',str(e.args)
    try:
        if rollback_id:
            logger.debug("开始回滚版本至ID:{}".format(rollback_id))
            cmd = "{act} 'sh /root/sh/all.sh rollout \"{app_name}\" \"{tag}\" '".format(
                act = REMOTE_ACTION,
                app_name = app['app_name'],
                tag = rollback_id
            )
        else:
            logger.debug("确定构建环境为:" + app['env_str'])
            cmd = "{act} 'sh /root/sh/all.sh \"{env}\" \"{app_name}\" \"{app_build}\" \"{app_git}\" \"{app_branch}\" \"{app_pakage}\"'".format(
                act = REMOTE_ACTION,
                app_name=app['app_name'],
                app_build=app['app_build'],
                app_git=app['app_git'],
                app_branch=app['app_branch'],
                env=app['env_str'],
                app_pakage=app['app_pakage']
            )
        ret, _ = common.sub_call(cmd, pid_file=spec_name, logger=logger)
        task_node = TaskModel.objects.get(pk=task_id)
        if not ret:
            code = task_node.SUCCESS
            logger.debug("远程命令执行成功")
            # d = aliy_docker_api.MyDockerControl(app_name=app['app_name'])
            # for i in range(1, 4):
            #     logger.debug("重置容器环境,TRY {}".format(i))
            #     cd, pu = d.ops_app(action='redeploy')
            #     if cd == 200 or cd == 202:
            #         logger.debug(pu);
            #         code = task_node.SUCCESS
            #         break
            #     else:
            #         logger.error("重置失败,返回HTTP CODE为{},错误内容包含{}".format(cd, pu))
            #         code = task_node.FAIL
            #     sleep(5)
            # logger.debug(d.query_app(qk=["current_state", "updated"]))
        else:
            code = task_node.FAIL
            logger.error("远程命令执行失败")
    except Exception as exc:
        code = task_node.FAIL
        logger.error(str(exc.args))
    finally:
        app_node = task_node.task_app
        app_node.app_status = app_node.READY
        app_node.save()
        task_node.task_log = f_file_name  ##日志数据过大，采取读取文件方式实现
        task_node.task_status = int(code)
        task_node.save()
        return '构建完成，执行结果为{}'.format( code )



def remote_exc_sql(sql_id, db_config, sql_file):
    # (10, 'SQL提交，等待审核'),
    # (20,'通过审核，等待执行'),
    # (30,'审核拒绝，打回'),
    # (40,'SQL执行中'),
    # (50,'执行失败'),
    # (60,'执行成功'),
    # (70,'撤回'),
    instance = SqlNode.objects.get(pk=sql_id)
    try:
        import os
        os.system('dos2unix {}'.format(sql_file))
        with common.dbExec(db_config) as db_conn:
            with open(sql_file) as f:
                # if 'use' or 'USE' or 'definer' or 'DEFINER' in f.read():
                #     instance.stat = 50
                #     instance.log = 'SQL脚本中，无需指定数据库，请勿添加 "use db;"' \
                #                    '存储过程请勿添加 "definer"' \
                #                    '请勿添加"注释"，以防报错'
                #     ret = 1
                # else:
                SQL = f.read()
                run_sql = []
                result = []
                for sql_row in SQL.split(';'):
                    if sql_row:
                        run_sql.append(sql_row.replace('\n', ''))
                count = 0
                for run in run_sql:
                    if run:
                        result_title = []
                        db_conn.execute(run)
                        data = db_conn.fetchall()
                        if data:
                            result_index = db_conn.description
                            for title in result_index:
                                result_title.append(title[0])
                            result.append(result_title)
                            for row in data:
                                d_list = []
                                for d in row:
                                    d_list.append(str(d))
                                result.append(d_list)
                        else:
                            count = count + 1
                            count_data = db_conn.rowcount
                            data_list = []
                            res_title = '第{}个语句，执行结果：'.format(count)
                            data_list.append(res_title)
                            data_list.append('影响{}行数据'.format(count_data))
                            result.append(data_list)
                result_row = str(result).replace('\'', '\"')
                instance.log = result_row
                instance.stat = 60
                ret = 0
    except Exception as e:
        instance.stat = 50
        instance.log = str(e.args)
        ret = 1
    finally:
        instance.save()
        return ret



