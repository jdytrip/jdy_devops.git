from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkecs.request.v20140526 import DescribeInstancesRequest
from aliyunsdkecs.request.v20140526 import StopInstanceRequest
from django.views import generic
from ..models import *
from django.http import HttpResponseRedirect
from django.urls import reverse
import json, re
from DevopsManager import settings
from django.shortcuts import render
from ManagerMaster.views import tasks
from django.views import generic



RANDOM_NUMBER = 199999090122
class CmdbIndex(generic.ListView):
    '''项目组展示'''
    template_name = "cmdb_index.html"
    context_object_name = "cmdb_list"
    model = Cmdb

    def get_context_data(self, **kwargs):
        context = super(CmdbIndex, self).get_context_data(**kwargs)
        return context

def cmdb_index(request):
    """
    /server/cmdb/<int:project_id>/list/
    CMDB首页
    """
    comm_re = re.compile(reverse('ManagerMaster:cmdb_list',args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    cmdb_list = []
    url_list = request.session['authority']
    for url in url_list:
        gen_pid = comm_re.search(url)
        # print(gen_pid)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if Cmdb.objects.filter(pk=pid).exists():
                cmdb_list.append(pid)
    if cmdb_list:
        cmdb_list = list(set(cmdb_list))
        return HttpResponseRedirect(reverse('ManagerMaster:cmdb_list',
                                            args=(cmdb_list[0],)))
    error = {
        'title': '无法找到相关公有云',
        'code': 404,
        'msg': "无法找到相关公有云"
    }
    return render(request, 'error.html', {'error': error,
                                          'active': 'cmdb_admin'})
def cmdb_list(request,cmdb_id):
    cmdb_node = Cmdb.objects.get(pk=cmdb_id)
    type = cmdb_node.type
    return_dict = {}
    if type == 1:
        region = ['cn-hangzhou', 'cn-shanghai', 'cn-beijing', 'cn-hongkong', 'us-west-1']
        cmdb_list = []
        comm_re = re.compile(reverse('ManagerMaster:cmdb_list', args=(RANDOM_NUMBER,)
                                     ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
        for url in request.session['authority']:
            gen_pid = comm_re.search(url)
            if gen_pid:
                pid = int(gen_pid.group(1))
                if cmdb_id == pid:
                    continue
                try:
                    cmdb_list.append(
                        Cmdb.objects.get(pk=pid)
                    )
                except:
                    pass
                continue
            try:
                a = comm_re.search(url)
                pid, aid = a.group(1), a.group(2)
                if int(pid) == cmdb_id:
                    cmdb_list.append(Cmdb.objects.get(pk=int(aid)))
            except:
                pass
        return_dict = {'cmdb_node': cmdb_node,
                       'cmdb_list': cmdb_list
                       }

        client_msg = Cmdb.objects.get(pk=cmdb_id)
        Data = []
        for __region in region:
            client = AcsClient(
                client_msg.access_key_id,
                client_msg.access_key_secret,
                # 'cn-shenzhen'
                __region
            );


            req = DescribeInstancesRequest.DescribeInstancesRequest()
            req.set_PageSize(100)
            req.set_PageNumber(1)
            req.set_accept_format('json')
            response = client.do_action_with_exception(req)
            jsondata = json.loads(response)

            for list in jsondata['Instances']['Instance']:
                print(list)
                InstanceName = list['InstanceName']
                Memory = list['Memory']
                Cpu = list['Cpu']
                InstanceNetworkType = list['InstanceNetworkType']
                if list['PublicIpAddress']['IpAddress']:
                    PublicIpAddress = list['PublicIpAddress']['IpAddress'][0]
                else:
                    PublicIpAddress = 'None'
                if InstanceNetworkType == 'vpc':
                    # try:
                    #     IpAddress = list['NetworkInterfaces']['NetworkInterface'][0]['PrimaryIpAddress']
                    # except:
                    IpAddress = list['VpcAttributes']['PrivateIpAddress']['IpAddress'][0]
                    # IpAddress = None
                else:
                    IpAddress = list['InnerIpAddress']['IpAddress'][0]
                ZoneId = list['ZoneId']
                Status = list['Status']
                data = {'InstanceName': InstanceName,
                        'Memory': Memory,
                        'Cpu': Cpu,
                        'PublicIpAddress': PublicIpAddress,
                        'InstanceNetworkType': InstanceNetworkType,
                        'PrimaryIpAddress': IpAddress,
                        'ZoneId': ZoneId,
                        'Status': Status
                        }
                Data.append(data)

        return_dict = dict(return_dict)
        return_dict['data'] = Data
    return render(request, 'cmdb_list.html', return_dict)



def cmdb_edit(request, cmdb_id=None):
    if cmdb_id:
        cmdb_obj = Cmdb.objects.get(pk=int(cmdb_id))
    else:
        cmdb_obj = None
    if request.method == "POST":
        if cmdb_obj:
            form = CmdbForm(request.POST, instance=cmdb_obj)
        else:
            form = CmdbForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                # 新增项目需要生成各类权限数据
                if not cmdb_obj:
                    p_name = form.cleaned_data['name']
                    cmdb_obj = Cmdb.objects.get(name=p_name)
                    p_level_one = GlobalAuthority.objects.get(type=GlobalAuthority.SERVER, url=settings.CMDB_URL)
                    cmdb_url = settings.CMDB_ID_URL.format(cmdb_id=cmdb_obj.id)
                    GlobalAuthority.objects.create(name=p_name, parent=p_level_one, url=cmdb_url, type=GlobalAuthority.
                                                   SERVER)
                    # gen_auths = []
                    # myself = request.user
                    # for i in GlobalAuthority.objects.filter(type=GlobalAuthority.SERVER):
                    #     gen_auths.append(UserAuthority(user=myself, authority=i))
                    # UserAuthority.objects.bulk_create(gen_auths)
                    #
                    # # 根据权限树规则刷新数据库权限表记录
                    # tasks.update_db_authority.delay()
                    # # 根据权限表数据刷新缓存中所有用户的权限记录
                    # tasks.update_cache_authority.delay()
                return HttpResponseRedirect(reverse('ManagerMaster:cmdb_manage_index'))
            except Exception as ex:
                if 'UNIQUE' in ','.join(str(i) for i in ex.args):
                    form.add_error('name', "存在重复数据:{}".format(str(ex.args)))
                else:
                    form.add_error('name', str(ex.args))
        return render(request, 'cmdb_edit.html', {'form': form, 'cmdb_obj': cmdb_obj})
    if cmdb_obj:
        form = CmdbForm(instance=cmdb_obj)
    else:
        form = CmdbForm()
    return render(request, 'cmdb_edit.html',
                  {'form': form,'cmdb_obj':cmdb_obj})