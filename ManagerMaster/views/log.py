from django.shortcuts import render
from DevopsManager import settings
from django.urls import reverse
from django.http import FileResponse
import os
import re
import datetime
from ..models import *
from django.http import HttpResponseRedirect
RANDOM_NUMBER = 199999090122

def log_list(request,project_id):
    """显示指定项目组下APP日志
        /ManagerMaster/project/<int:project_id>/app/log/
        /ManagerMaster/project/<int:project_id>/app/<int:app_id>/log/
    """
    project_node = ProjectModel.objects.get(pk=project_id)
    user = request.user
    app_list =  []
    project_list = []
    comm_re = re.compile(reverse('ManagerMaster:log_list', args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    comm_re_one = re.compile(reverse('ManagerMaster:app_log',args=(RANDOM_NUMBER,RANDOM_NUMBER)
                                     ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    for url in request.session['authority']:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if project_id == pid:
                continue
            try:
                project_list.append(
                    ProjectModel.objects.get(pk=pid)
                )
            except:
                pass
            continue
        try:
            a = comm_re_one.search( url )
            pid, aid = a.group(1),a.group(2)
            if int(pid) == project_id:
                app_list.append(AppModel.objects.get(pk=int(aid)))
        except:
            pass
    return render(request,'log_list.html',{'project_node':project_node,
                                           'app_list':app_list,
                                           'project_list':project_list})

def app_log(request,project_id,app_id):
    '''查看指定项目下的日志文件'''
    app_node = AppModel.objects.get(pk=app_id)
    app_name = app_node.app_name
    app_log_path = os.path.join( settings.SERVER_LOG_PATH, app_name)
    if request.method == "GET" and 'download' in request.GET:
        download_file = request.GET['download'].strip()
        type = request.GET['type'].strip()
        if type:
            the_file_name = os.path.join( app_log_path,type,download_file)
        else:
            the_file_name = os.path.join( app_log_path,download_file)
        # return StreamingHttpResponse(file_iterator(the_file_name))
        f = open(the_file_name, 'rb')
        response = FileResponse(f, content_type='APPLICATION/OCTET-STREAM')  # 文件头设定可以让任意文件都能正确下载
        response['Content-Disposition'] = 'attachment;filename="{}"'.format(
            os.path.basename(download_file))
        response['Content-Length'] = os.path.getsize(the_file_name)
        return response

    file_list = []
    if os.path.isdir( app_log_path):
        for root,dirs,files in os.walk(app_log_path):
            ddir = root.split(app_log_path)[1].strip(os.sep)
            if files:
                for f in files:
                    real_f = os.path.join( root, f)
                    file_list.append(
                        {
                            'name' : f,
                            'dir' : ddir,
                            'mtime' : datetime.datetime.fromtimestamp(
                                os.path.getmtime(real_f)).strftime('%F %T'),
                            'size' : "{:.2f}".format(os.path.getsize(real_f)/1024), #单位为K
                        }
                    )
    return render(request,'app_log.html',{'file_list':file_list,'app_node':app_node,
                                          'project_id':project_id })


def log_index(request):
    """APP日志显示首页
    /ManagerMaster/project/<int:project_id>/app/log/
    /ManagerMaster/project/<int:project_id>/app/<int:app_id>/log/
    """
    comm_re = re.compile(reverse('ManagerMaster:log_list', args=(RANDOM_NUMBER,)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    pro_list = []
    url_list = request.session['authority']
    for url in url_list:
        gen_pid = comm_re.search(url)
        if gen_pid:
            pid = int(gen_pid.group(1))
            if ProjectModel.objects.filter(pk=pid).exists():
                pro_list.append(pid)
    if pro_list:
        pro_list = list(set(pro_list))
        return HttpResponseRedirect(reverse('ManagerMaster:log_list',
                                            args=(pro_list[0],)))
    error = {
        'title': '无法找到相关项目',
        'code': 404,
        'msg': "无法找到相关的项目"
    }
    return render(request, 'error.html', {'error': error,
                                          'active': 'project_admin'})

# def db_index(request):
#     """db展示首页
#     根据不同项目组进行分类，根据权限确认项目及db展示
#     /ManagerMaster/data/<int:project_id>/<int:db_id>/detail/
#     """
#     proj_list = []
#     proj_list = ProjectModel.objects.values('id','project_name')