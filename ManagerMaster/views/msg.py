from django.shortcuts import render
from ..models import *
from django.http import HttpResponseRedirect
RANDOM_NUMBER = 199999090122

def message_list(request):
    name = request.user.username
    if name == 'admin':
        di = {
            'message_list': message.objects.order_by('type')
        }
    else:
        di = {
            'message_list': message.objects.filter(name=name).all()
        }
    d = request.GET.get('d')
    if d:
        try:
            di = dict(json.loads( d ), **di)

        except:
            pass
    return render(request, 'message_index.html', di)

def message_add(request):
    if request.method=="POST":
        type = 1
        title = request.POST.get('title')
        msg = request.POST.get('msg')
        if not title or not msg:
            return render(request, 'message_add.html',
                          {'error': '不能为空',
                           'title': title, 'msg': msg})
        else:
            try:
                info = message.objects.create(
                    name=request.user.username,
                    type=type,
                    title=title,
                    msg=msg
                )
                info.save()
            except Exception as exc:
                return render(request, 'message_index.html',
                              {'error': exc.args,
                               'title': title, 'msg': msg})

            return HttpResponseRedirect('/message/index/')
    return render(request, 'message_add.html')


def message_detail(request, pk):
    di = {
        'message_detail': message.objects.get(pk=pk)
    }
    d = request.GET.get('d')
    if d:
        try:
            di = dict(json.loads( d ), **di)
        except:
            pass
    return render(request, 'message_detail.html', di)

def message_del(request, pk):
    msg = message.objects.get(pk=pk)
    if msg:
        try:
            message.objects.get(pk=pk).delete()
        except:
            pass
    return HttpResponseRedirect('/message/index/')

def message_change(request, pk, type):
    if type:
        try:
            message.objects.filter(pk=pk).update(type=type)
        except:
            pass
    return HttpResponseRedirect('/message/index/')


def _exp_get_msg(request):
    msg = request.GET.get('msg')
    pass