import os, sys
import time

from subprocess import Popen, PIPE, STDOUT
import logging
from contextlib import contextmanager


class myLogger():
    def __init__(self, user, file_name, level=logging.DEBUG):
        logger = logging.getLogger(user)
        logger.setLevel(level)
        fomatter = logging.Formatter('%(asctime)s -%(name)s-%(levelname)s:%(message)s')

        ch = logging.StreamHandler()
        ch.setLevel(level)
        ch.setFormatter(fomatter)
        logger.addHandler(ch)

        fh = logging.FileHandler(file_name)
        fh.setLevel(level)
        fh.setFormatter(fomatter)
        logger.addHandler(fh)
        self.logger = logger

    def debug(self,msg):
        self.logger.debug(msg)

    def info(self, msg):
        self.logger.info(msg)

    def error(self, msg):
        self.logger.error(msg)


def sub_call(cmd, pid_file=None, logger=None,
            *popenargs, **kwargs):
    myenv = os.environ.copy()
    myenv["COLUMNS"] = "512"
    if os.name != 'posix':
        return 110,'不是posix系统，暂不支持操作'
    p = Popen(cmd, shell=True,
              stdin=PIPE,
              stdout=PIPE,
              stderr=STDOUT,
              env=myenv,
              close_fds=True,  # 关闭除0，1，2外的文件描述符，而无需等待fork出的子进程是否完成
              preexec_fn=os.setpgrp,
              universal_newlines=True,
              *popenargs, **kwargs
              )
    if pid_file:
        with open(pid_file, 'w') as f:
            f.write('%i' % p.pid)
    if logger: # 日志文件句柄则不返回日志
        while p.poll() is None:
            # time.sleep(0.1)
            ss = p.stdout.readline().strip()
            if ss:
                logger.debug(ss)
        ss = p.stdout.read().strip()
        logger.debug(ss)
        output = ""
        retcode = p.returncode
    else:
        output, errput = p.communicate()
        if errput:
            output = output + errput
        retcode = p.returncode
        if logger:
            if retcode:
                logger.error(output)
            else:
                logger.debug(output)
    try:
        os.remove(pid_file)
    except:
        pass
    return retcode, output

import pymysql.err


@contextmanager
def dbExec(kwargs={}):
    import pymysql
    if not kwargs:
        raise ValueError('连接参数为空')
    if 'NAME' in kwargs:
        db = kwargs['NAME']
    else:
        db = None
    if 'charset' in kwargs:
        charset = kwargs['charset']
    else:
        charset = 'utf8'
    db = pymysql.connect(host=kwargs['HOST'], user=kwargs['USER'],
                           port=int(kwargs['PORT']), passwd=kwargs['PASSWORD'],
                           db=db, charset=charset
                           )
    try:
        cursor = db.cursor()
        yield cursor
    except Exception as e:
        db.rollback()
        raise pymysql.err.ProgrammingError(str(e.args))
    else:
        db.commit()