from django.shortcuts import render
from DevopsManager import settings
from django.urls import reverse
from ..models import *
from django.http import HttpResponseRedirect
from django.views import generic
from ManagerMaster.views import tasks


class ServerProjectIndex(generic.ListView):
    '''项目组展示'''
    template_name = "project_index.html"
    context_object_name = "project_list"
    model = ProjectModel

    def get_context_data(self, **kwargs):
        context = super(ServerProjectIndex, self).get_context_data(**kwargs)
        return context

class MyTreeNode():
    '''生成权限树'''
    def __init__(self,  data={}):
        self.data = data
        self.children = []
        self.parent_node = None

    def to_dict(self,l=[]):
        d= {"text": self.data['text'],
            "nId": self.data['nId'],
            "tags": self.data['tags']}
        if self.children:
            d['nodes'] = [child_node.to_dict(l) for child_node in self.children]
        if l and self.data['nId'] in l:
            d['state'] = {'checked': True}
        return d

    @classmethod
    def get_tree_data(cls,check_l = []):
        tree_data = []
        for node in GlobalAuthority.objects.filter(parent=None):
            top_node = cls.gen_node(None, node)
            tree_data.append(top_node)
        dict_data = [td.to_dict(check_l) for td in tree_data]
        return json.dumps(dict_data, ensure_ascii=False)

    @classmethod
    def gen_node(cls, parent_node, current_node):
        d = {"text": current_node.name,
             "nId": current_node.id,
             "tags": [current_node.url]}
        node = MyTreeNode(d)
        children = GlobalAuthority.objects.filter(parent=current_node)
        node.parent_node = parent_node
        if parent_node:
            parent_node.children.append(node)
        if not children:
            return node
        else:
            for child in children:
                cls.gen_node(node, child)
        return node

def account_authority(request, user_id):
    '''用户权限控制'''
    user = User.objects.get(pk=user_id)
    user_auth_node = UserAuthority.objects.filter(user=user)
    if 'authority_edit' in request.POST:
        authority_ids = request.POST.get('authority_ids')
        try:
            d = {'type': 'success', 'msg': '修改权限成功'}
            user_auth_node.delete()  #### 清空原有的权限数据
            for au_id in authority_ids.strip().split(','):
                try:
                    au_id = int(au_id)
                    auth_node = GlobalAuthority.objects.get(id=au_id)
                    UserAuthority.objects.create(user=user,
                                                 authority=auth_node)
                except Exception as exc:
                    continue
            try:
                # 根据权限树刷新用户权限表
                tasks.update_db_authority.delay()
                # 根据权限表数据刷新缓存中所有用户的权限记录
                tasks.update_cache_authority.delay()
            except:
                pass
        except Exception as ex:
            d = {'type': 'error', 'msg': '{}'.format(ex)}
        return HttpResponseRedirect('{}?d={}'.format(reverse('ManagerMaster:account_index'),json.dumps(d)))
    user_auth_list = [ i["authority"] for i in user_auth_node.values('authority') ]
    node_list = []
    for nid, name, url, pid in GlobalAuthority.objects.values_list('id','name','url','parent_id'):
        d = {'id': nid, 'pId': pid,'name': name + '[{}]'.format(url)}
        if nid in user_auth_list:
            d['checked'] = True
        else:
            d['checked'] = False
        node_list.append(d)
    return render(request, 'account_authority.html',
                  {'user': user, 'user_auth_list': user_auth_list,
                   'ztree_js': json.dumps(
                       node_list, ensure_ascii=False)})

def project_edit(request, project_id=None):
    '''新增或者修改项目组'''
    if project_id:
        proj_obj = ProjectModel.objects.get(pk=int(project_id))
    else:
        proj_obj = None
    if request.method == "POST":
        if proj_obj:
            form = ProjectForm(request.POST, instance=proj_obj)
        else:
            form = ProjectForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                # 新增项目需要生成各类权限数据
                if not proj_obj:
                    p_name = form.cleaned_data['project_name']
                    proj_obj = ProjectModel.objects.get(project_name=p_name)
                    p_level_one = GlobalAuthority.objects.get(type=GlobalAuthority.SERVER,url=settings.PROJECT_URL)
                    project_url = settings.PROJECT_ID_URL.format(project_id=proj_obj.id)
                    p_level_two = GlobalAuthority.objects.create( name=p_name, parent=p_level_one,
                                                                  url=project_url, type=GlobalAuthority.PROJECT,
                                                                  link_project=proj_obj)
                    api_url = project_url+'api/'
                    p_level_three = GlobalAuthority.objects.create( name='api监控',parent= p_level_two,
                                                                    url=api_url,type=GlobalAuthority.PROJECT,
                                                                    link_project=proj_obj)
                    son_list = []
                    son_list.append( GlobalAuthority(name='删除指定监控项',url=api_url+'delete/',
                                                     parent=p_level_three,type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj))
                    son_list.append( GlobalAuthority(name='查看所有监控项',url=api_url+'index/',
                                                     parent=p_level_three,type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj))
                    son_list.append( GlobalAuthority(name='修改或新增指定监控项',url=api_url+'edit/',
                                                     parent=p_level_three,type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj))
                    son_list.append( GlobalAuthority(name='新增app',url=project_url+'app/add/',
                                                     parent=p_level_two,type=GlobalAuthority.PROJECT,
                                                     link_project=proj_obj))
                    GlobalAuthority.objects.bulk_create( son_list )
                    gen_auths = []

                    myself = request.user
                    for i in GlobalAuthority.objects.filter(type=GlobalAuthority.PROJECT,
                                                            link_project=proj_obj):
                        gen_auths.append( UserAuthority( user=myself, authority= i ))

                    UserAuthority.objects.bulk_create( gen_auths)
                    try:
                        # 新增数据库管理相关权限
                        db_father = GlobalAuthority.objects.get(url='/server/db/', type=GlobalAuthority.SERVER)
                        p_level_two = GlobalAuthority.objects.create(name=p_name, parent=db_father,
                                                                 url='/server/db/{}/'.format(proj_obj.id),
                                                                 type=GlobalAuthority.PROJECT,
                                                                 link_project=proj_obj
                                                                     )
                        GlobalAuthority.objects.create(name='显示项目所属db', url='/server/db/{}/list/'.format(proj_obj.id),
                                                       parent=p_level_two, type=GlobalAuthority.PROJECT,
                                                       link_project=proj_obj
                                                       )
                    except:
                        print('创建数据库权限失败')

                    # 根据权限树规则刷新数据库权限表记录
                    tasks.update_db_authority.delay()
                    # 根据权限表数据刷新缓存中所有用户的权限记录
                    tasks.update_cache_authority.delay()

                return HttpResponseRedirect(reverse('ManagerMaster:project_index'))
            except Exception as ex:
                if 'UNIQUE' in ','.join(str(i) for i in ex.args):
                    form.add_error('project_name', "存在重复数据:{}".format(str(ex.args)))
                else:
                    form.add_error('project_name', str(ex.args))
        return render(request, 'project_edit.html', {'form': form,'proj_obj':proj_obj})
    if proj_obj:
        form = ProjectForm(instance=proj_obj)
    else:
        form = ProjectForm()
    return render(request, 'project_edit.html',
                  {'form': form,'proj_obj':proj_obj})

def server_project_delete(request, project_id):
    '''删除项目'''
    try:
        p = ProjectModel.objects.get(id=project_id)
        try:
            MonitorApi.objects.filter(project=p).update(stat=MonitorApi.STOPING)
        except:
            pass
        p.delete()
        d = {'type': 'success', 'msg': '删除成功'}
        # 根据权限表数据刷新缓存中所有用户的权限记录
        tasks.update_cache_authority.delay()
    except Exception as exc:
        d = {'type': 'error', 'msg': str(exc.args)}
    finally:
        return HttpResponseRedirect('{}?msg={}'.format(reverse('ManagerMaster:project_index'),json.dumps(d)))