from django.shortcuts import render
from django.urls import reverse
from django.template import Context, Template
import os
import re
from ..models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from . import tasks
RANDOM_NUMBER = 199999090122


class DbNodeIndex(generic.ListView):
    '''数据库管理'''
    template_name = "dn_index.html"
    context_object_name = "dn_list"
    model = DbNode

    def get_context_data(self, **kwargs):
        context = super(DbNodeIndex, self).get_context_data(**kwargs)
        return context



def dn_edit(request,dn_id=None):
    '''数据库用户新增或修改'''
    if dn_id:
        node = DbNode.objects.get(pk=dn_id)
        users = node.user
    else:
        node = None
        users = None
    user_list = DbUser.objects.values('id','name')
    if request.method == "POST":
        if node:
            form = DbNodeForm(request.POST,instance=node)
        else:
            form = DbNodeForm(request.POST)
        if form.is_valid():
            is_enough = False
            if not node:
                ds_node = form.cleaned_data['d_s']
                if ds_node.max_dbs <= ds_node.dbs:
                    form.add_error('d_s', '此数据库实例数据库已经使用满，无法再创建数据库')
                    is_enough = True
            if not is_enough:
                try:
                    form.save()
                    if not node:
                        db_name = form.cleaned_data['name']
                        db_node = DbNode.objects.get(name=db_name)
                        ds_node = db_node.d_s
                        ds_node.dbs += 1
                        ds_node.save()
                        proj_id = db_node.project_id
                        p_level_two = GlobalAuthority.objects.get(url='/server/db/{}/'.format(proj_id),
                                                                     type=GlobalAuthority.PROJECT)
                        base_url = '/server/db/{}/{}/'.format(proj_id, db_node.id)
                        p_level_three = GlobalAuthority.objects.create(name=db_node.name, url=base_url,
                                                                       parent=p_level_two, type=GlobalAuthority.PROJECT,
                                                                       link_db=db_node)
                        son_list = []
                        son_list.append(GlobalAuthority(name='新增sql', url=base_url + 'add/',
                                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                                        link_db=db_node))
                        son_list.append(GlobalAuthority(name='显示db所属sql', url=base_url + 'list/',
                                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                                        link_db=db_node))
                        son_list.append(GlobalAuthority(name='审核db所属sql', url=base_url + 'examine/',
                                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                                        link_db=db_node))
                        son_list.append(GlobalAuthority(name='查看sql', url=base_url + 'detail/',
                                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                                        link_db=db_node))
                        son_list.append(GlobalAuthority(name='审核查看sql', url=base_url + 'select/',
                                                        parent=p_level_three, type=GlobalAuthority.PROJECT,
                                                        link_db=db_node))
                        GlobalAuthority.objects.bulk_create(son_list)
                        # 优先刷新当前创建者权限缓存
                        tasks.update_cache_authority.delay([request.user.id])
                        # 根据权限树规则刷新数据库权限表记录
                        tasks.update_db_authority.delay()
                        # 根据权限表数据刷新缓存中所有用户的权限记录
                        tasks.update_cache_authority.delay()
                    return HttpResponseRedirect(reverse('ManagerMaster:dn_index'))
                except Exception as ex:
                    form.add_error('name',str(ex.args))
        return render(request,'dn_edit.html',{'form':form,'node':node,'users':users,
                                              'user_list':user_list})
    else:
        if node:
            form = DbNodeForm(instance=node)
        else:
            form = DbNodeForm()
    return render(request,'dn_edit.html', {'form': form,'user_list': user_list, 'users': users})

def dn_delete(request,dn_id):
    '''删除数据库用户'''
    try:
        db_node = DbNode.objects.get(pk=dn_id)
        ds_node = db_node.d_s
        db_node.delete()
        ds_node.dbs -= 1
        ds_node.save()
        # 根据权限表数据刷新缓存中所有用户的权限记录
        tasks.update_cache_authority.delay()
        return HttpResponse(json.dumps({'code': 0, 'msg': "删除成功"}))
    except Exception as e:
        return HttpResponse(json.dumps({'code': 1, 'msg': str(e.args)}))

class DbUserIndex(generic.ListView):
    '''数据库用户管理'''
    template_name = "dbuser_index.html"
    context_object_name = "dbuser_list"
    model = DbUser

    def get_context_data(self, **kwargs):
        context = super(DbUserIndex, self).get_context_data(**kwargs)
        return context

def dbuser_query(request):
    '''根据ids查询数据库用户名，主要用于展示数据库'''
    if request.method == "GET" and 'ids' in request.GET:
        ids = request.GET.get('ids')
        try:
            d_query = DbUser.objects.filter(id__in=ids.split(',')).values('name')
            return HttpResponse(json.dumps(list(d_query)))
        except:
            pass
    return HttpResponse()

def dbuser_edit(request,user_id=None):
    '''数据库用户新增或修改'''
    if user_id:
        dbuser_node = DbUser.objects.get(pk=user_id)
    else:
        dbuser_node =None
    if request.method == "POST":
        if dbuser_node:
            form = DbUserForm(request.POST,instance=dbuser_node)
        else:
            form = DbUserForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return HttpResponseRedirect(reverse('ManagerMaster:dbuser_index'))
            except Exception as ex:
                form.add_error('name',str(ex.args))
        return render(request,'dbuser_edit.html',{'form': form, 'node': dbuser_node})
    else:
        if dbuser_node:
            form = DbUserForm(instance=dbuser_node)
        else:
            form = DbUserForm()
    return render(request, 'dbuser_edit.html', {'form': form})

def dbuser_delete(request, user_id):
    '''删除数据库用户'''
    try:
        DbUser.objects.get(pk=user_id).delete()
        return HttpResponse(json.dumps({'code': 0, 'msg': "删除成功"}))
    except Exception as e:
        return HttpResponse(json.dumps({'code': 1, 'msg': str(e.args)}))

class DataServicesIndex(generic.ListView):
    '''数据库实例管理'''
    template_name = "ds_index.html"
    context_object_name = "ds_list"
    model = DataServices

    def get_context_data(self, **kwargs):
        context = super(DataServicesIndex, self).get_context_data(**kwargs)
        return context

def ds_edit(request,ds_id=None):
    '''数据库实例新增或修改'''
    if ds_id:
        ds_node = DataServices.objects.get(pk=ds_id)
        print( ds_id )
    else:
        ds_node =None
    if request.method == "POST" :
        if ds_node:
            form = DataServicesForm(request.POST, instance=ds_node)
        else:
            form = DataServicesForm(request.POST)
        if form.is_valid():
            form.save()
            try:
                form.save()
                return HttpResponseRedirect(reverse('ManagerMaster:ds_index'))
            except Exception as ex:
                form.add_error('name', str(ex.args))
    else:
        if ds_node:
            form = DataServicesForm(instance=ds_node)
        else:
            form = DataServicesForm()
    return render(request, 'ds_edit.html', {'form': form})

def ds_delete(request,ds_id):
    '''删除数据库实例'''
    try:
        DataServices.objects.get(pk=ds_id).delete()
        return HttpResponse(json.dumps({'code': 0, 'msg': "删除成功"}))
    except Exception as e:
        return HttpResponse(json.dumps({'code': 1, 'msg': str(e.args)}))

def sql_examine(request,proj_id,db_id,sql_id):
    """执行审核操作"""
    # (10, 'SQL提交，等待审核'),
    # (20,'通过审核，等待执行'),
    # (30,'审核拒绝，打回'),
    # (40,'SQL执行中'),
    # (50,'执行失败'),
    # (60,'执行成功'),
    # (70,'撤回'),
    if request.method == "GET" and 'act' in request.GET:
        act = int(request.GET.get('act', 0))
        sql_node = SqlNode.objects.get(pk=sql_id)
        if act:
            sql_node.stat = act
            if act in [20, 30]:
                sql_node.leader = request.user.username
            sql_node.save()
            if act == 20:
                file_name = sql_node.sql.name
                db = sql_node.db
                db_users = DbUser.objects.filter(type=sql_node.type) # 保持类型的一致
                real_db_user = None
                for du_id in db.user.strip().split(','):
                    try:
                        real_db_user = db_users.get(pk=int(du_id))
                        break
                    except:
                        pass
                if real_db_user:
                    db_config = {
                            'NAME': db.db_name,
                            'USER': real_db_user.user,
                            'PASSWORD': real_db_user.password,
                            'HOST': db.d_s.host,
                            'PORT': db.d_s.port
                        }
                    sql_node.stat = 40
                    sql_node.save()
                    tasks.exc_sql.delay(sql_id, db_config, file_name)
                else:
                    sql_node.stat = 60
                    sql_node.log = '无法获取到有效的数据库用户'
                    sql_node.save()
    return HttpResponse(json.dumps({'code':0}))

def sql_detail(request,proj_id,db_id,sql_id):
    """查看sql详情"""
    sql_node = SqlNode.objects.get(pk=sql_id)
    if request.method == "GET":
        if "content" in request.GET:
            file_name = sql_node.sql.name
            if not os.path.isfile(file_name):
                return HttpResponse('无法找到SQL文件{}'.format(file_name))
            try:
                t = Template("{{log| escape| linebreaks}}")
                # t = Template("{{log|safe}}")
                with open(file_name) as f:
                    d = {'log': f.read()}
                    return HttpResponse(t.render(Context(d)))
            except TypeError:
                return HttpResponse('文件类型不匹配')
            except Exception as e:
                return HttpResponse('错误:{}'.format(str(e.args)))
        elif 'log' in request.GET:
            return HttpResponse(sql_node.log)
        elif 'rollback' in request.GET:  # 撤回
            rollback_id = int(request.GET.get('rollback', 0))
            if rollback_id == 70 and request.user.username == sql_node.author:
                sql_node.stat = 70
                sql_node.save()
                return HttpResponse(json.dumps({'code': 0}))
            else:
                return HttpResponse(json.dumps({'code': 1}))


    proj_name = ProjectModel.objects.get(pk=proj_id).project_name
    return render(request,'sql_detail.html',{
        'proj_id': proj_id,
        'db_id': db_id,
        'sql_node': sql_node,
        'proj_name': proj_name,
        'history': json.loads(sql_node.acts)
    })

def sql_list(request,proj_id,db_id):
    """db页中所有sql列表"""
    db_node = DbNode.objects.get(pk=db_id)
    proj_name = ProjectModel.objects.get(pk=proj_id).project_name
    if request.method == "GET" and 'examine' in request.GET:
        sql_list = SqlNode.objects.filter(db_id=db_id, stat=10)
    else:
        # sql_list = SqlNode.objects.filter(db_id=db_id).exclude(stat=70).order_by('-id')
        sql_list = SqlNode.objects.filter(db_id=db_id).order_by('-id')
    return render(request,'sql_list.html',{
        'sql_list': sql_list,
        'db_node': db_node,
        'proj_name': proj_name,
        'proj_id': proj_id
    })

def sql_edit(request,proj_id,db_id):
    """新增sql，禁止修改"""
    db_node = DbNode.objects.get(pk=db_id)
    if request.method == "POST":
        form = SqlNodeForm(request.POST.copy(), request.FILES)
        if form.is_valid():
            node = form.save(commit=False)
            node.author = request.user.username
            node.db = db_node
            node.save()
            return HttpResponseRedirect(reverse("ManagerMaster:sql_list", args=(proj_id,db_id)))
    else:
        form = SqlNodeForm()
    return render(request,'sql_edit.html',{
        'form': form, 'db_node': db_node,
        'proj_id': proj_id
    })


def db_list(request, proj_id):
    """列出指定项目下所有权限的db项"""
    comm_re = re.compile(reverse('ManagerMaster:sql_list', args=(proj_id, RANDOM_NUMBER)
                                 ).replace(str(RANDOM_NUMBER), '([0-9]+)'))
    tmp_db_list = []
    db_list = []
    url_list = request.session['authority']
    for url in url_list:
        db_id = comm_re.search(url)
        if db_id:
            real_db_id = int(db_id.group(1))
            if DbNode.objects.filter(pk=real_db_id).exists():
                tmp_db_list.append(real_db_id)
    if tmp_db_list:
        tmp_db_list = list(set(tmp_db_list))
        db_list = DbNode.objects.filter(pk__in=tmp_db_list)
        # for db_id in tmp_db_list:
        #     db_dict = model_to_dict(DbNode.objects.get(pk=db_id))
        #     db_dict['num'] = SqlNode.objects.filter(db_id=db_id,stat=10).count()
        #     db_list.append( db_dict )
        proj_node = ProjectModel.objects.get(pk=proj_id)
        return render(request, 'db_list.html', {'db_list': db_list,
                                                'proj_node': proj_node})
    else:
        error = {
            'title': '无法找到所属数据库',
            'code': 403,
            'msg': "没有数据库在你的管理下，请联系管理员"
        }
        return render(request, 'error.html', {'error': error,
                                          'active': 'project_admin'})

