from django.shortcuts import render
import os
import re
import datetime
from ..models import *
from django.core.cache import cache



def server_index(request):
    '''首页展示'''
    '''统计数'''
    now_mon = datetime.datetime.now().month
    now_year = datetime.datetime.now().year
    user_key = 'user_count'
    user_value = cache.get(user_key)
    proj_key = 'proj_count'
    proj_value = cache.get(proj_key)
    app_key = 'app_count'
    app_value = cache.get(app_key)
    task_key = 'task_count'
    task_value = cache.get(task_key)
    login_key = 'login_count'
    login_value = cache.get(login_key)
    build_key = 'build_count'
    build_value = cache.get(build_key)
    pod_key = 'pod_count'
    pod_value = cache.get(pod_key)
    node_key = 'node_count'
    node_value = cache.get(node_key)
    mem_key = 'mem_count'
    mem_value = cache.get(mem_key)
    cpu_key = 'cpu_count'
    cpu_value = cache.get(cpu_key)

    if user_value == None:
        cache.set(user_key, json.dumps(User.objects.filter().count()), 3600)
        user_count = cache.get(user_key)
    else:
        user_count = user_value
    if proj_value == None:
        cache.set(proj_key, json.dumps(ProjectModel.objects.filter().count()), 3600)
        proj_count = cache.get(proj_key)
    else:
        proj_count = proj_value
    if app_value == None:
        cache.set(app_key, json.dumps(AppModel.objects.filter().count()), 3600)
        app_count = cache.get(app_key)
    else:
        app_count = app_value
    if task_value == None:
        cache.set(task_key, json.dumps(TaskModel.objects.filter(task_etime__year=now_year).filter(task_etime__month=now_mon).count()), 60)
        task_count = cache.get(task_key)
    else:
        task_count = task_value
    if login_value == None:
        cache.set(login_key, User.objects.values('username', 'last_login').order_by('-last_login')[0:10], 60)
        login_count = cache.get(login_key)
    else:
        login_count = login_value
    if build_value == None:
        cache.set(build_key, TaskModel.objects.values('task_app_id__app_name', 'task_person', 'task_stime', 'task_etime', 'task_status').order_by('-task_stime')[0:10], 60)
        build_count = cache.get(build_key)
    else:
        build_count = build_value

    if pod_value == None:
        kube_pod_data = os.popen('kubectl get pod | grep -v NAME')
        pod_data = []
        for pod_d in kube_pod_data:
            pod_data.append(pod_d)
        cache.set(pod_key, len(pod_data), 60)
        pod_count = cache.get(pod_key)
    else:
        pod_count = cache.get(pod_key)
    if node_value == None:
        kube_node_data = os.popen('kubectl get node | grep -v NAME')
        node_data = []
        for node_d in kube_node_data:
            node_data.append(node_d)
        cache.set(node_key, len(node_data), 60)
        node_count = cache.get(node_key)
    else:
        node_count = cache.get(node_key)
    if cpu_value == None or mem_value == None:
        water_data = os.popen('kubectl top node | grep -v NAME')
        node_cpu = 0
        node_mem = 0
        for _data in water_data:
            l_data = _data.split()
            node_cpu = node_cpu + int(re.sub("\D", "", l_data[1]))
            node_mem = node_mem + int(re.sub("\D", "", l_data[3]))
        total_dict = json.load(os.popen('kubectl get node -o json'))
        cpu = 0
        mem = 0
        for total in total_dict['items']:
            cpu = cpu + int(total['status']['allocatable']['cpu'])
            mem = mem + int(re.sub("\D", "", total['status']['allocatable']['memory']))
        cache.set(cpu_key, '%.2f' % ((node_cpu / 1000) / cpu * 100), 60)
        cache.set(mem_key, '%.2f' % (node_mem / (mem / 1000) * 100), 60)
        cpu_count = cache.get(cpu_key)
        mem_count = cache.get(mem_key)
    else:
        cpu_count = cache.get(cpu_key)
        mem_count = cache.get(mem_key)


    '''展示登录用户'''
    contaxt = {
        'login_list': login_count,
        'user_count': user_count,
        'proj_count': proj_count,
        'app_count': app_count,
        'task_count': task_count,
        'build_count': build_count,
        'pod_count': pod_count,
        'node_count': node_count,
        'cpu_count': cpu_count,
        'mem_count': mem_count
    }
    return render(request, 'server_index.html', contaxt)