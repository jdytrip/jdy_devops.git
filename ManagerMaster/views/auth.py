from django.shortcuts import render
from DevopsManager import settings
from django.urls import reverse
from django.db.models import Q
from django.db.models import ObjectDoesNotExist
from ManagerMaster.models import *
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject
from django.contrib.auth import get_user

from django.core.cache import cache
from ManagerMaster.views import tasks

EXCLUDE_URL = ['/test/']
RANDOM_NUMBER = 199999090122

class MyAuthenticationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """鉴权中间件"""
        assert hasattr(request, 'session'), (
                                                "The Django authentication middleware requires session middleware "
                                                "to be installed. Edit your MIDDLEWARE%s setting to insert "
                                                "'django.contrib.sessions.middleware.SessionMiddleware' before "
                                                "'django.contrib.auth.middleware.AuthenticationMiddleware'."
                                            ) % ("_CLASSES" if settings.MIDDLEWARE is None else "")
        request.user = SimpleLazyObject(lambda: get_user(request))
        _login_path = settings.LOGIN_URL
        global_common_U = [ _login_path,
                            reverse('ManagerMaster:account_logout'),
                            reverse('ManagerMaster:account_register'),
                            reverse('ManagerMaster:account_c_password')
                            ]
        if not request.user.is_authenticated and request.path not in global_common_U :
            if _login_path != request.path:
                return HttpResponseRedirect( _login_path )
            return HttpResponseRedirect('?next={}'.format(_login_path,request.path))
        # 通过登录验证的请求从缓存中获取权限列表,并更新session
        authority_list = cache.get('user_{}'.format(request.user.id))
        request.session['authority'] = authority_list

        # 测试连接则直接跳过鉴权
        if EXCLUDE_URL:
            for url in EXCLUDE_URL:
                if request.path.startswith(url):
                    return

        # 登录成功则直接鉴权
        # 登录、登出、注册、修改个人信息、首页为通用地址，不做鉴权处理
        g_l = global_common_U.append(reverse('ManagerMaster:index'))
        if request.path not in global_common_U:
            ## 登录成功则直接鉴权
            is_authority = False
            if authority_list:
                for a_l in authority_list:
                    # 验证规则
                    # 被验证的地址只有包含有权限列表中某元素则认为验证通过，
                    # eg:请求地址/server/task/15/log/,权限列表包含/server/task/15/,则验证通过
                    if request.path.startswith(a_l):
                        is_authority = True
                        break
            if not is_authority:
                return render(request,'403.html')
    # def process_response(self, request, response):
    #     if request.user:
    #         key = 'user_' + str(request.user.id)
    #         request.session['authority'] = cache.get(key)
    #     return response
    # handlers.py 181行data=data.encode()


def account_c_password(request):
    """找回密码"""
    error_token = ""
    if request.method == "POST" and 'user_id' in request.POST and 'reset_password_token' in request.POST:
        c_password = 1
        token = request.POST.get('reset_password_token')
        user_id = cache.get(token)
        if user_id == int(request.POST.get('user_id')):
            password = request.POST.get('user_password')
            password2 = request.POST.get('user_password2')
            if password != password2:
                return render(request, 'account_c_password.html', {
                    'type': "error",
                    'msg': "两次密码不一致",
                    'c_password' : c_password
                })
            try:

                u = User.objects.get(pk=user_id)
                u.set_password( password )
                u.save()
                try:
                    cache.delete_pattern(token)
                except:
                    pass
                d = {'success':"重置密码成功，请登录" }
                return HttpResponseRedirect('{}?msg='.format(reverse('auth:account_login'),
                                                             json.dumps(d,ensure_ascii=False)))
            except ObjectDoesNotExist:
                error_token = "用户不存在，请确定是否被删除"
            except Exception as e:
                error_token = str(e.args)
        else:
            error_token = "非法的请求,{} != {}".format(user_id,request.POST.get('user_id'))
            # error_token = "非法的请求"

    if 'reset_password_token' in request.GET:
        c_password = 1
        token = request.GET.get('reset_password_token')
        if token:
            user_id = cache.get(token)
            if user_id:
                if request.method == "GET" :
                    try:
                        user_id = User.objects.get(pk=user_id).id
                        return render(request, 'account_c_password.html',
                                      {'c_password': c_password, 'user_id': user_id,'reset_password_token':token})
                    except ObjectDoesNotExist:
                        error_token = "用户不存在，请确定是否被删除"
                    except Exception as e:
                        error_token = str( e.args )
            else:
                error_token = "重置链接地址无效或者过期"
        else:
            error_token = "无效的重置链接地址"
    if error_token:
        return render(request, 'account_c_password.html', {'error_token': error_token, 'c_password': c_password })

    return render(request,'account_c_password.html')


def account_login(request):
    """登录及找回密码验证"""
    if request.method == "POST" and 'c_name' in request.POST:
        type = "error"
        msg = ""
        c_name = request.POST.get('c_name').strip()
        try:
            user = User.objects.get(Q(username=c_name) | Q(email=c_name))
            from_mail = settings.EMAIL_HOST_USER
            from random import sample
            from string import ascii_letters, digits
            random_str = "token" + "".join(sample(ascii_letters + digits, 15)) + str(user.id)
            href = '{}{}?reset_password_token={}'.format(settings.LINSTEN_ADDR.rstrip('/'),
                                                         reverse('auth:account_c_password'), random_str)
            # 后台发送重置邮件
            msg = '''<html>
    <body>
    <div style = "width: 610px;height: 509px;background-color: #f9f9f9;">
        <div style="width: 100%;height: 426px;background-color: #ffffff;padding-left: 35px;padding-right: 35px;">
            <div style="color: #22a2a1;font-family: Lato;font-weight: 600;font-size: 20px;letter-spacing: 1px;padding-top: 39px;">
                金豆云运维平台
            </div>
            <div style="font-size: 17px;color: rgba(19, 36, 63, 0.8);font-family: Lato;font-weight: 400;
            line-height: 23px;margin-top: 16px;max-width: 538px;">
            </div>
            <div style="font-size: 48px;color: #13243f;font-family: Helvetica Neue;
            font-style: normal;font-weight: 300;margin: 32px auto 32px;text-align: center;">
                密码重置
            </div>
            <div style="font-size: 17px;color: #13243f;font-family: Lato;font-weight: 400;
            max-width: 538px;line-height: 23px;margin-bottom: 19px;">
                您好 ''' + user.username + '''！您已经请求了重置密码，可以点击下面的链接来重置密码。重置链接10分钟之内有效。
    <br/>
                <a href="''' + href + '''" style="color: #219290;font-family: Lato;font-weight: 600;">''' + href + '''</a>
                <span style="color: #101a30;font-family: Lato;font-weight: 400;"><br/>如果您没有请求重置密码，请忽略这封邮件。在您点击上面链接修改密码之前，您的密码将会保持不变。</span>
            </div>
            <div style="font-size: 17px;color: #13243f;font-family: Lato;font-weight: 400;">
                By ''' + from_mail + '''
            </div>
        </div>
    </div>
    </body>
    </html>'''
            # 使用缓存存储，10分钟后直接销毁
            cache.set(random_str, user.id, timeout=10*60)
            tasks.send_mail.delay(subject='金豆云运维平台重置密码信息', msg=msg, to=[user.email], is_html=True)
            type = "success"
            msg = "请至邮箱查收重置密码邮件"
        except ObjectDoesNotExist:
            msg = "找不到此用户名或邮箱"
        except Exception as e:
            msg = str(e.args)
        return render(request, 'account_c_password.html', {'type': type, 'msg': msg})
    next = request.GET.get('next', '')
    if 'account_login' in request.POST:
        name = request.POST.get('user_name')
        password = request.POST.get('user_password')
        user = authenticate(request, username=name, password=password)
        if user is not None:
            login(request, user)
            key = 'user_{}'.format(request.user.id)
            value = tasks.gen_auth_list(request.user.id)
            cache.set(key, value, timeout=None)
            if not next:
                next = reverse('ManagerMaster:index')
            return HttpResponseRedirect(next)
        else:
            return render(request, 'account_login.html', {'next': next,'error': '用户验证失败，请确认'})
    return render(request, 'account_login.html', {'next': next})

def account_logout(request):
    '''登出'''
    if request.user.is_authenticated:
        try:
            del request.session['authority']
        except KeyError:
            pass
        logout(request)
    return HttpResponseRedirect(settings.LOGIN_URL)

def account_change(request):
    '''修改个人账户信息'''
    act = ''
    try:
        act = request.GET.get('act')
    except:
        pass
    username = request.user.username
    user = User.objects.get(username=username)
    if request.method == "POST" and 'account_change' in request.POST:
        type = "error"
        msg = ""
        if act == 'password':
            try:
                old_password = request.POST.get('old_password')
                password = request.POST.get('user_password')
                password2 = request.POST.get('user_password2')
                if user.check_password(old_password):
                    if password != old_password:
                        if password == password2:
                            user.set_password( password )
                            user.save()
                            login(request, user) # 重新登录一次，避免需要重新登录
                            type = "success"
                            msg = "密码修改完成"
                        else:
                            msg = "新密码和确定密码不一致"
                    else:
                        msg = "新密码和原密码相同"
                else:
                    msg = "旧密码错误"
            except Exception as ex:
                msg = str(ex.args)
        else:
            user_email = request.POST.get('user_email',"")
            password = request.POST.get('old_password',"")
            if user.check_password( password):
                if user_email:
                    user.email = user_email
                    try:
                        user.save()
                        type = "success"
                        msg = "修改个人信息成功"
                    except Exception as e:
                        msg = str(e.args)
                else:
                    msg = "邮箱信息不能为空"
            else:
                msg = "密码错误"
        return render(request, 'account_change.html', {'user': user, 'type': type, 'msg': msg, 'act': act})
    return render(request,'account_change.html',{'user': user, 'act': act})

def account_index(request):
    '''用户管理首页'''
    di = {
        'user_list': User.objects.order_by('pk').order_by('-is_active')
    }
    d = request.GET.get('d')
    if d:
        try:
            di = dict(json.loads( d ), **di)
        except:
            pass
    return render(request, 'account_index.html', di)

def account_active(request,user_id):
    '''是否允许用户登录，0为关闭，1为打开'''
    if request.method == "GET" and 'is_active' in request.GET:
        is_active=int(request.GET.get('is_active'))
        user = User.objects.get(id=user_id)
        if is_active:
            user.is_active=True
        else:
            user.is_active=False
        user.save()
    return HttpResponseRedirect(reverse('index:account_index'))

def account_register(request):
    '''用户注册'''
    if 'account_register' in request.POST:
        user_name = request.POST.get('user_name')
        user_password = request.POST.get('user_password')
        user_password2 = request.POST.get('user_password2')
        user_email = request.POST.get('user_email')
        if user_password != user_password2:
            return render(request, 'account_register.html',
                          {'error': '两次密码不一致',
                           'user_name': user_name, 'user_email': user_email})
        if not user_name or not user_password or not user_email:
            return render(request, 'account_register.html',
                          {'error': '不能存在选项为空',
                           'user_name': user_name, 'user_email': user_email})
        try:
            user = User.objects.create_superuser(
                username=user_name,
                password=user_password,
                email=user_email
            )
            user.save()
        except Exception as exc:
            return render(request, 'account_register.html',
                          {'error': exc.args,
                           'user_name': user_name, 'user_email': user_email})
        return render( request,'account_register.html',
                       {'success':"注册完成，请联系管理员审核"})
    return render(request, 'account_register.html')

def account_edit(request, user_id):
    '''用户信息修改'''
    user = get_object_or_404(User, pk=user_id)
    if 'account_edit' in request.POST:
        password = request.POST.get('user_password')
        email = request.POST.get('user_email')
        if not password:
            return render(request,'account_edit.html',
                          {'user': user, 'type': 'error', 'msg': '密码不能为空'})
        try:
            user.set_password(password)
            user.email = email
            user.save()
            return render(request, 'account_edit.html',
                          {'user': user, 'type': 'success', 'msg': '修改成功'})
        except Exception as exc:
            return render(request, 'account_edit.html',
                          {'user': user, 'type': 'error', 'msg': '修改失败：{}'.format(exc)})
    return render(request, 'account_edit.html',
                  {'user': user})

def account_delete(request, user_id):
    '''删除用户'''
    try:
        User.objects.get(id=user_id).delete()
        key = 'user_{}'.format(user_id)
        cache.delete_pattern( key )
        d = {'type': 'success', 'msg': '删除成功'}
    except Exception as exc:
        d = {'type': 'error', 'msg': '{}'.format(exc)}
    finally:
        return HttpResponseRedirect( '{}?d={}'.format(reverse('index:account_index'),json.dumps(d)))