from django import template
import json
register = template.Library()


@register.filter(name='mo')
def mo(value,arg):
    try:
        if int(value) % int(arg) == 0:
            return True
        else:
            return False
    except:
        return False

@register.filter(name='in_list')
def in_list(value,list):
    if value in list:
        return True    
    return False

@register.filter(name='json_matter')
def json_matter(value):
    from json import loads,dumps
    d = loads(value)
    return dumps(d,indent=4)


import datetime
#register.tag('current_time', do_current_time)


class CurrentTimeNode(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string
    def render(self, context):
        return datetime.datetime.now().strftime(self.format_string)



@register.tag(name="current_time")
def do_current_time(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise template.TemplateSyntaxError("%r tag's argument should be in quotes" % tag_name)
    return CurrentTimeNode(format_string[1:-1])


class upperNode(template.Node):
    def __init__(self,nodelist):
        self.nodelist = nodelist

    def render(self, context):
        content = self.nodelist.render(context)
        return content.upper()

@register.tag
def upper(parser,token):
    nodelist = parser.parse("endupper") #指定结束符
    parser.delete_first_token()

    return upperNode(nodelist)


from django.template import Node
from django.template.base import kwarg_re
from django.utils.html import conditional_escape
from django.template.base import TemplateSyntaxError


class URLNode(Node):
    def __init__(self, view_name, args, kwargs, asvar):
        self.view_name = view_name
        self.args = args
        self.kwargs = kwargs
        self.asvar = asvar
        self.myargs = args

    def render(self, context):
        from django.urls import reverse, NoReverseMatch
        args = [arg.resolve(context) for arg in self.args]
        ###### include数据
        inclu_list = args[-1]

        #if not isinstance(inclu_list,tupple) or not isinstance(inclu_list,list):
        #    raise ValueError('搜索字段不是列表或数组')
        args = args[:-1]

        kwargs = {k: v.resolve(context) for k, v in self.kwargs.items()}
        view_name = self.view_name.resolve(context)



        try:
            current_app = context.request.current_app
        except AttributeError:
            try:
                current_app = context.request.resolver_match.namespace
            except AttributeError:
                current_app = None
        # Try to look up the URL. If it fails, raise NoReverseMatch unless the
        # {% url ... as var %} construct is used, in which case return nothing.
        url = ''
        try:
            url = reverse(view_name, args=args, kwargs=kwargs, current_app=current_app)
        except NoReverseMatch:
            if self.asvar is None:
                raise

        if self.asvar:
            context[self.asvar] = url
            return ''
        else:
            if context.autoescape:
                url = conditional_escape(url)

            if inclu_list and url not in inclu_list:
                return ''
            return url

@register.tag
def myurl(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' takes at least one argument, a URL pattern name." % bits[0])
    viewname = parser.compile_filter(bits[1])
    args = []
    kwargs = {}
    asvar = None
    bits = bits[2:]
    if len(bits) >= 2 and bits[-2] == 'as':
        asvar = bits[-1]
        bits = bits[:-2]

    if len(bits):
        for bit in bits:
            match = kwarg_re.match(bit)
            if not match:
                raise TemplateSyntaxError("Malformed arguments to url tag")
            name, value = match.groups()
            if name:
                kwargs[name] = parser.compile_filter(value)
            else:
                args.append(parser.compile_filter(value))
    return URLNode(viewname, args, kwargs, asvar)

from django.template.defaulttags import NodeList
from django.template.smartif import OPERATORS,EndToken,infix
from django.template.defaulttags import TemplateLiteral

def my_in_func(context,x, y):
    '''被验证的地址只有包含有权限列表中某元素则认为验证通过，如请求地址为/server/task/15/log/，而权限列表里面存在/server/task/15/，则我们认为验证通过'''
    a = x.eval(context)
    for b in y.eval(context):
        if b in a:
            return True
    return False

#OPERATORS['in'] = infix(9, lambda context, x, y: x.eval(context) in y.eval(context))
OPERATORS['in'] = infix(9, my_in_func)
OPERATORS['not in'] = infix(9, lambda context, x, y: x.eval(context) not in y.eval(context))

# Assign 'id' to each:
for key, op in OPERATORS.items():
    op.id = key

class IfParser:
    error_class = ValueError

    def __init__(self, tokens):
        # Turn 'is','not' and 'not','in' into single tokens.
        num_tokens = len(tokens)
        mapped_tokens = []
        i = 0
        while i < num_tokens:
            token = tokens[i]
            if token == "is" and i + 1 < num_tokens and tokens[i + 1] == "not":
                token = "is not"
                i += 1  # skip 'not'
            elif token == "not" and i + 1 < num_tokens and tokens[i + 1] == "in":
                token = "not in"
                i += 1  # skip 'in'
            mapped_tokens.append(self.translate_token(token))
            i += 1
        self.tokens = mapped_tokens
        self.pos = 0
        self.current_token = self.next_token()

    def translate_token(self, token):
        try:
            op = OPERATORS[token]
        except (KeyError, TypeError):
            return self.create_var(token)
        else:
            return op()

    def next_token(self):
        if self.pos >= len(self.tokens):
            return EndToken
        else:
            retval = self.tokens[self.pos]
            self.pos += 1
            return retval

    def parse(self):
        retval = self.expression()
        # Check that we have exhausted all the tokens
        if self.current_token is not EndToken:
            raise self.error_class("Unused '%s' at end of if expression." %
                                   self.current_token.display())
        return retval

    def expression(self, rbp=0):
        t = self.current_token
        self.current_token = self.next_token()
        left = t.nud(self)
        while rbp < self.current_token.lbp:
            t = self.current_token
            self.current_token = self.next_token()
            left = t.led(left, self)
        return left

    def create_var(self, value):
        return Literal(value)


class TemplateIfParser(IfParser):
    error_class = TemplateSyntaxError

    def __init__(self, parser, *args, **kwargs):
        self.template_parser = parser
        super().__init__(*args, **kwargs)

    def create_var(self, value):
        return TemplateLiteral(self.template_parser.compile_filter(value), value)


@register.tag('my_if')
def do_if(parser, token):
    # {% if ... %}
    bits = token.split_contents()[1:]
    nodelist = parser.parse(('elif', 'else', 'endif'))
    conditions_nodelists = [(bits, nodelist)]
    token = parser.next_token()

    # {% else %} (optional)
    if token.contents == 'else':
        nodelist = parser.parse(('endif',))
        conditions_nodelists.append((None, nodelist))
        token = parser.next_token()

    # {% endif %}
    if token.contents != 'endif':
        raise TemplateSyntaxError('Malformed template tag at line {0}: "{1}"'.format(token.lineno, token.contents))


    return MyIfNode(conditions_nodelists,parser)



class MyIfNode(Node):

    def __init__(self, conditions_nodelists,parser):
        self.conditions_nodelists = conditions_nodelists
        self.parser =parser

    def __repr__(self):
        return '<%s>' % self.__class__.__name__

    def __iter__(self):
        for _, nodelist in self.conditions_nodelists:
            yield from nodelist

    @property
    def nodelist(self):
        return NodeList(node for _, nodelist in self.conditions_nodelists for node in nodelist)

    def render(self, context):
        for bits, nodelist in self.conditions_nodelists:
            if bits:
                i = bits.index('in')
                real_bits = bits[i:]
                args_list = [eval(j) for j in bits[:i]]
                args = [self.parser.compile_filter(arg).resolve(context) for arg in args_list[1:]]
                view_name = args_list[0]
                from django.urls import reverse, NoReverseMatch
                try:
                    current_app = context.request.current_app
                except AttributeError:
                    try:
                        current_app = context.request.resolver_match.namespace
                    except AttributeError:
                        current_app = None
                try:
                    url = reverse(view_name, args=args, kwargs={}, current_app=current_app)
                except NoReverseMatch:
                    raise
                if context.autoescape:
                    url = conditional_escape(url)
                real_bits.insert(0, '"{}"'.format(url))
                condition = TemplateIfParser(self.parser, real_bits).parse()
            else:
                condition = None
            if condition is not None:           # if / elif clause
                try:
                    match = condition.eval(context)

                except VariableDoesNotExist:
                    match = None
            else:                               # else clause
                match = True

            if match:
                return nodelist.render(context)
        return ''


@register.simple_tag
def json_pod(pod_monitor, app_name, pod_type):
    try:
        cpu = []
        mem = []
        for pod_data in pod_monitor:
            if pod_data['name'] == app_name:
                cpu.append(pod_data['cpu'])
                mem.append(pod_data['mem'])
        cpu = ','.join(cpu)
        mem = ','.join(mem)
        if pod_type == 'cpu':
            return cpu
        elif pod_type == 'mem':
            return mem
        else:
            pass
    except Exception:
        pass



