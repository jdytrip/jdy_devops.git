from django import template
register = template.Library()

@register.filter(name='in_list')
def in_list(value,list):
    if value in list:
        return True    
    return False

import datetime
#register.tag('current_time', do_current_time)


class CurrentTimeNode(template.Node):
    def __init__(self, format_string):
        self.format_string = format_string
    def render(self, context):
        return datetime.datetime.now().strftime(self.format_string)



@register.tag(name="current_time")
def do_current_time(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise template.TemplateSyntaxError("%r tag's argument should be in quotes" % tag_name)
    return CurrentTimeNode(format_string[1:-1])


class upperNode(template.Node):
    def __init__(self,nodelist):
        self.nodelist = nodelist

    def render(self, context):
        content = self.nodelist.render(context)
        return content.upper()

@register.tag
def upper(parser,token):
    nodelist = parser.parse("endupper") #指定结束符
    parser.delete_first_token()

    return upperNode(nodelist)