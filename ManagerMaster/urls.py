from django.urls import path, re_path

#from . import views
from .views import index, auth, database, apps, api, log, manage, msg, cmdb

app_name = 'ManagerMaster'
urlpatterns = [
    # 开放权限管理
    path('index/', index.server_index, name="index"),
    path('common/account/login/', auth.account_login, name='account_login'),
    path('common/account/logout/', auth.account_logout, name='account_logout'),
    path('common/account/change/', auth.account_change, name='account_change'),
    path('common/account/change/password/',auth.account_c_password, name='account_c_password'),
    path('common/account/register/', auth.account_register, name='account_register'),

    path('server/project/api/index/', api.api_index, name='api_index'),
    path('server/project/app/index/', apps.app_index, name='app_index'),
    path('server/project/log/index/', log.log_index, name='log_index'),

    # 用户管理
    path('admin/account/', auth.account_index,name='account_index'),
    path('admin/account/authority/<int:user_id>/', manage.account_authority,name='account_authority'),
    path('admin/account/edit/<int:user_id>/', auth.account_edit, name='account_edit'),
    path('admin/account/active/<int:user_id>/', auth.account_active, name='account_active'),
    path('admin/account/delete/<int:user_id>/', auth.account_delete, name='account_delete'),
    # 数据库实例管理
    path('admin/db/ds/', database.DataServicesIndex.as_view(), name="ds_index"),
    path('admin/db/ds/edit/', database.ds_edit, name="ds_edit"),
    path('admin/db/ds/edit/<int:ds_id>/', database.ds_edit, name="ds_edit"),
    path('admin/db/ds/delete/<int:ds_id>/', database.ds_delete, name="ds_delete"),
    # 数据库用户管理
    path('admin/db/user/', database.DbUserIndex.as_view(), name="dbuser_index"),
    path('admin/db/user/edit/', database.dbuser_edit, name="dbuser_edit"),
    path('admin/db/user/edit/<int:user_id>/', database.dbuser_edit, name="dbuser_edit"),
    path('admin/db/user/query/', database.dbuser_query, name="dbuser_query"),
    path('admin/db/user/delete/<int:user_id>/', database.dbuser_delete, name="dbuser_delete"),
    # 数据库管理
    path('admin/db/dn/', database.DbNodeIndex.as_view(), name="dn_index"),
    path('admin/db/dn/edit/', database.dn_edit, name="dn_edit"),
    path('admin/db/dn/edit/<int:dn_id>/', database.dn_edit, name="dn_edit"),
    path('admin/db/dn/delete/<int:dn_id>/', database.dn_delete, name="dn_delete"),

    # 项目管理
    path('admin/project/', manage.ServerProjectIndex.as_view(), name="project_index"),
    path('admin/project/edit/', manage.project_edit, name="project_edit"),
    path('admin/project/edit/<int:project_id>/', manage.project_edit, name="project_edit"),
    path('admin/project/<int:project_id>/delete/', manage.server_project_delete, name="project_delete"),

    # 服务管理
    path('server/project/<int:project_id>/app/add/', apps.app_add, name="app_add"),
    path('server/project/<int:project_id>/app/list/', apps.app_list, name="app_list"),
    path('server/project/<int:project_id>/app/log/', log.log_list, name="log_list"),
    # app管理
    path('server/project/<int:project_id>/app/<int:app_id>/detail/', apps.app_detail, name="app_detail"),
    path('server/project/<int:project_id>/app/<int:app_id>/edit/', apps.app_edit, name="app_edit"),
    path('server/project/<int:project_id>/app/<int:app_id>/delete/', apps.app_delete, name="app_delete"),
    path('server/project/<int:project_id>/app/<int:app_id>/log/', log.app_log, name='app_log'),
    # app执行
    path('server/project/<int:project_id>/app/<int:app_id>/task/log/', apps.task_log, name="task_log"),
    path('server/project/<int:project_id>/app/<int:app_id>/task/add/', apps.task_add, name="task_add"),
    path('server/project/<int:project_id>/app/<int:app_id>/task/rollback/', apps.task_rollback, name="task_rollback"),
    # api监控
    path('server/project/<int:project_id>/api/index/', api.api_detail, name="api_detail"),
    path('server/project/<int:project_id>/api/edit/', api.api_edit, name="api_edit"),
    path('server/project/<int:project_id>/api/delete/', api.api_delete, name="api_delete" ),

    path('server/db/<int:proj_id>/list/',database.db_list, name='db_list'),
    path('server/db/<int:proj_id>/<int:db_id>/add/', database.sql_edit, name='sql_edit'),
    path('server/db/<int:proj_id>/<int:db_id>/list/', database.sql_list, name='sql_list'),
    path('server/db/<int:proj_id>/<int:db_id>/examine/<int:sql_id>/', database.sql_examine, name='sql_examine'),
    path('server/db/<int:proj_id>/<int:db_id>/select/<int:sql_id>/', database.sql_examine, name='sql_select'),
    path('server/db/<int:proj_id>/<int:db_id>/detail/<int:sql_id>/', database.sql_detail, name='sql_detail'),

    # 留言
    path('message/index/', msg.message_list, name="message"),
    path('message/add/', msg.message_add, name="message_add"),
    path('message/detail/<int:pk>/', msg.message_detail, name="message_detail"),
    path('message/del/<int:pk>/', msg.message_del, name="message_del"),
    path('message/change/<int:pk><int:type>/', msg.message_change, name="message_change"),


    # CMDB
    path('admin/cmdb/', cmdb.CmdbIndex.as_view(), name="cmdb_manage_index"),
    path('server/cmdb/index/', cmdb.cmdb_index, name="cmdb_index"),
    path('server/cmdb/<int:cmdb_id>/', cmdb.cmdb_list, name="cmdb_list"),
    path('admin/cmdb/edit/', cmdb.cmdb_edit, name="cmdb_edit"),
    path('admin/cmdb/edit/<int:cmdb_id>/', cmdb.cmdb_edit, name="cmdb_edit"),


    # path('test/', msg.test, name="test"),

]
