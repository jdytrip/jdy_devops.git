from django import forms
from django.db import models

# Create your models here.

from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

class MyUserManager(BaseUserManager):
    def create_user(self, username,email,
                    password=None):
        if not username:
            raise ValueError('用户名不能为空')
        user = self.model(
            username=username,
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,username,email,
                         password=None ):
        user = self.create_user(username, email,password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    username = models.CharField(max_length=50,unique=True)
    email = models.EmailField(max_length=255,unique=True)
    is_active = models.BooleanField(default=False)


    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    def __str__(self):
        return self.username
    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        db_table = 'user'


class ProjectModel(models.Model):
    project_name = models.CharField(max_length=50,
                                    unique=True, verbose_name="项目名")
    project_explain = models.TextField(max_length=100, blank=True,
                                       verbose_name="说明")

    def __str__(self):
        return self.project_name

    class Meta:
        db_table = 'project'

class ProjectForm(forms.ModelForm):
    class Meta:
        model = ProjectModel
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(ProjectForm, self).__init__(*args, **kwargs)

class AppModel(models.Model):
    NODEJS = 0
    JAR = 1
    WAR = 2
    ENV_TYPE_CHOICES = (
        (NODEJS, 'NODEJS'),
        (JAR, 'JAVA JAR'),
        (WAR, 'JAVA WAR'),
    )
    READY = 100
    WAIT = 0
    ING = 1
    STAT_CHOICE=(
        (READY, '准备完毕'),
        (WAIT, '等待后台执行'),
        (ING, '执行中'),
    )
    app_name = models.CharField(max_length=50, verbose_name="应用名称")
    app_env = models.SmallIntegerField(choices=ENV_TYPE_CHOICES,verbose_name="编译环境",
                                       default=JAR)
    app_pakage = models.CharField(max_length=255, verbose_name="包路径")
    app_git = models.CharField(max_length=100, verbose_name="git地址")
    app_branch = models.CharField(max_length=50, verbose_name="git分支")
    app_project = models.ForeignKey('ProjectModel', null=True, verbose_name="所属项目",
                                    on_delete=models.CASCADE)
    app_build = models.CharField(max_length=50, verbose_name="build参数")
    app_status = models.IntegerField(choices=STAT_CHOICE,
                                     default=READY, verbose_name="是否处理发布状态")
    # 100为没有执行过任务,0为等待执行，1为执行中
    def __str__(self):
        return self.app_name

    class Meta:
        db_table = "app"
        unique_together = ('app_name', 'app_project')

class TaskModel(models.Model):
    ING=1
    SUCCESS=2
    FAIL=3
    STAT_CHOICE=(
        (ING, '进行中'),
        (SUCCESS, '成功'),
        (FAIL, '失败')
    )
    task_app = models.ForeignKey('AppModel', null=True,
                                 on_delete=models.SET_NULL,
                                 verbose_name="所属app")
    task_person = models.CharField(choices=STAT_CHOICE,max_length=255,null=True)
    task_status = models.SmallIntegerField(choices=STAT_CHOICE,default=0, verbose_name="任务状态")
    # 0为等待执行，1为开始执行，2为执行成功，3为执行失败
    task_log = models.TextField(max_length=65535, null=True,
                                verbose_name="任务日志")
    task_stime = models.DateTimeField(auto_now_add=True, verbose_name="任务创建时间")
    # 第一次创建时候会自动插入
    task_etime = models.DateTimeField(auto_now=True, verbose_name="任务结束时间")

    def __str__(self):
        return '{}-{}'.format(self.task_app.app_name, self.id)

    class Meta:
        db_table = "task"

class AppForm(forms.ModelForm):
    class Meta:
        model = AppModel
        exclude = ('app_project', 'app_status')
        # fields = "__all__"

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(AppForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['app_name'].widget.attrs['readonly'] = True
    def clean_app_name(self):
        instance = getattr(self,'instance',None)
        if instance and instance.pk:
            return instance.app_name
        else:
            return self.cleaned_data['app_name']




class message(models.Model):
    PENDING = 1
    PROCESSING = 2
    PROCESSED = 3
    TYPE_CHOICE = (
        (PENDING, '待处理'),
        (PROCESSING, '正在处理'),
        (PROCESSED, '已处理'),
    )
    name = models.CharField(max_length=50, verbose_name="名称")
    time = models.DateTimeField(auto_now_add=True, verbose_name="留言时间")
    type = models.SmallIntegerField(choices=TYPE_CHOICE, default=PENDING)
    title = models.CharField(max_length=50, verbose_name="标题")
    msg = models.TextField(max_length=65500, null=True, verbose_name="内容")
    class Meta:
        db_table = 'message'



class MonitorApi(models.Model):
    M_POST = 1
    M_GET = 2
    METHOD_CHOICE = (
        (M_POST, 'post'),
        (M_GET, 'get'),
    )
    E_RET = 1;E_OPUT = 2;E_ALL = 3;E_FAIL=4;R_ALL = 100
    ERROR_CHOICE = (
        (E_FAIL, '请求接口失败'),
        (E_RET, '状态码不匹配'),
        (E_OPUT, '返回值不匹配'),
        (E_ALL, '状态码和返回值都不匹配'),
        (R_ALL, '匹配'),
    )
    name = models.CharField(max_length=50,verbose_name="名称")
    cron = models.IntegerField(verbose_name="监控周期(单位:分钟)",default=1)
    mark = models.TextField(max_length=255,verbose_name="备注")
    project = models.ForeignKey('ProjectModel',verbose_name="所属项目",on_delete=models.CASCADE)
    method = models.SmallIntegerField(choices=METHOD_CHOICE,verbose_name="请求类型",default=M_GET)
    url = models.CharField(max_length=255,verbose_name="URL")
    params = models.TextField(max_length=255,verbose_name="参数(仅支持json格式)",default="{}")
    expect_ret = models.IntegerField(verbose_name="返回状态码",default=200)
    expect_oput = models.TextField(verbose_name="返回值(仅支持json格式)",default="{}")
    ret = models.IntegerField(verbose_name="实时状态码",null=True)
    oput = models.TextField(verbose_name="实时返回数据",default="{}")
    error = models.SmallIntegerField(choices=ERROR_CHOICE,null=True)
    update_time = models.DateTimeField(auto_now=True)

    RUNING = 1  # 正在执行
    STOPING = 2  # 正在停止
    START = 3  # 正在启动
    STOPED = 4  # 完全停止
    CHANGING = 5  # 等待改变
    STAT_CHOICE = ((RUNING,'正在执行'),(STOPING,'正在停止'),(START,'正在启动'),
                   (STOPED,'完全停止'),(CHANGING,'等待改变'))
    stat = models.SmallIntegerField(choices=STAT_CHOICE, default=START)
    class Meta:
        db_table = 'monitorapi'

class MonitorApiForm(forms.ModelForm):
    class Meta:
        model = MonitorApi
        exclude = ('project', 'ret', 'oput', 'error','stat','update_time')

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(MonitorApiForm, self).__init__(*args, **kwargs)


class DataServices(models.Model):
    '''数据库实例模型'''
    MYSQL = 1
    REDIS = 2
    TYPE_CHOICE = (
        (MYSQL, 'mysql'),
        (REDIS, 'redis')
    )
    name = models.CharField(verbose_name="实例名称",max_length=50,unique=True,help_text="")
    type = models.SmallIntegerField(choices=TYPE_CHOICE,verbose_name='数据库类型',default=MYSQL)
    host = models.CharField(verbose_name="Host",max_length=150)
    port = models.IntegerField(verbose_name="Port",default=3306)
    charset = models.CharField(verbose_name="编码",max_length=50,default="utf8")
    max_dbs = models.IntegerField(verbose_name="最大db数",default=10)
    dbs = models.IntegerField(default=0)
    mark = models.TextField(verbose_name="描述",max_length=150,null=True,blank=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('host', 'port')


class DataServicesForm(forms.ModelForm):
    class Meta:
        model = DataServices
        exclude = ('dbs',)

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(DataServicesForm, self).__init__(*args, **kwargs)

class DbUser(models.Model):
    '''数据库用户模型'''
    ALL = 0
    READER = 1
    TYPE_CHOICE = (
        (ALL, '修改'),
        (READER, '查询'),
    )
    name = models.CharField(verbose_name="名称", max_length=50,unique=True)
    user = models.CharField(verbose_name="User", max_length=25)
    password = models.CharField(verbose_name="Password", max_length=150,null=True,blank=True)
    type = models.SmallIntegerField(choices=TYPE_CHOICE, default=ALL)
    mark = models.TextField(verbose_name="描述", max_length=150,null=True,blank=True)

    def __str__(self):
        return self.name



class DbUserForm(forms.ModelForm):
    class Meta:
        model = DbUser
        fields = "__all__"

    def __init__(self, *args, **kwargs ):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(DbUserForm, self).__init__(*args, **kwargs)

class DbNode(models.Model):
    """
    DB模型
    mysql实例一对应多个db
    多个user对应多个db
    """
    name = models.CharField(verbose_name="名称", max_length=50, unique=True)
    d_s = models.ForeignKey('DataServices', verbose_name="数据库实例", on_delete=models.CASCADE)
    db_name = models.CharField(verbose_name="库名", max_length=25)
    user = models.CharField(verbose_name='', max_length=150,null=True, blank=True)
    project = models.ForeignKey('ProjectModel', verbose_name='所属项目', on_delete=models.CASCADE, default=None, null=True, blank=True)
    mark = models.TextField(verbose_name="描述", max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):
    #     ds_node = self.d_s
    #     if ds_node.dbs >= ds_node.max_dbs:
    #         raise ValueError('db pool is full')
    #     # ds_node.dbs += 1
    #     # ds_node.save()
    #     super(self.__class__, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('d_s', 'db_name')




class DbNodeForm(forms.ModelForm):
    class Meta:
        model = DbNode
        fields = "__all__"
        # exclude = ['user']

    def __init__(self, *args,**kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            if field_name == 'user':
                field.widget.attrs.update({'hidden': True})
            else:
                field.widget.attrs.update({'class': 'form-control'})
        super(DbNodeForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            # self.fields['project'].widget.attrs['readonly'] = True
            self.fields['project'].widget.attrs['disabled'] = True

    # def __init__(self, *args, **kwargs):
    #     super(DbNodeForm, self).__init__(*args, **kwargs)
    #     for field_name in self.fields:
    #         field = self.fields[field_name]
    #         if field_name == 'user':
    #             field.widget.attrs.update({'hidden': True,'class': 'form-control'})
    #         else:
    #             field.widget.attrs.update({'class': 'form-control'})
    # def __new__(cls, *args, **kwargs):
    #     obj = super(DbNodeForm, cls).__new__(cls, *args, **kwargs)
    #     for field_name in obj.base_fields:
    #         field = obj.base_fields[field_name]
    #         if field_name == 'user':
    #             field.widget.attrs.update({'hidden': 'true'})
    #         else:
    #             field.widget.attrs.update({'class': 'form-control'})


def _upload_to(instance, filename):
    # print(instance.name)
    import os, time, random
    # 切割文件名、扩展名
    fn, ext = os.path.splitext(filename)
    # 定义文件名，年月日时分秒随机数
    fn += '{}_{}'.format(time.strftime('%Y%m%d%H%M%S'), random.randint(0, 100))
    return os.path.join('upload_sql', fn + ext)


# from django.core.files.storage import FileSystemStorage
#
#
# class ImageStorage(FileSystemStorage):
#     from django.conf import settings
#
#     def __init__(self, location=settings.MEDIA_ROOT, base_url=settings.MEDIA_URL):
#         # 初始化
#         super(ImageStorage, self).__init__(location, base_url)
#
#     # 重写 _save方法
#     def _save(self, name, content):
#         import os, time, random
#         # 文件扩展名
#         ext = os.path.splitext(name)[1]
#         # 文件目录
#         d = os.path.dirname(name)
#         # 定义文件名，年月日时分秒随机数
#         fn = time.strftime('%Y%m%d%H%M%S')
#         fn = fn + '_%d' % random.randint(0, 100)
#         # 重写合成文件名
#         name = os.path.join(d, fn + ext)
#         # 调用父类方法
#         return super(ImageStorage, self)._save(name, content)

import json
class SqlNode(models.Model):
    """sql模型"""
    STAT = (
        (10,'SQL提交，等待审核'),
        (20,'通过审核，等待执行'),
        (30,'审核拒绝，打回'),
        (40,'SQL执行中'),
        (50,'执行失败'),
        (60,'执行成功'),
        (70,'撤回'),
    )
    name = models.CharField(verbose_name="用途", max_length=150)
    db = models.ForeignKey('DbNode', verbose_name='执行对象', on_delete=models.SET_NULL,
                           null=True, blank=True, default=None)
    sql = models.FileField(verbose_name='上传需要执行的sql', upload_to=_upload_to, max_length=150)
    type = models.SmallIntegerField(verbose_name='选择执行类型',
                                    choices=DbUser.TYPE_CHOICE, default=DbUser.READER)
    author = models.CharField(max_length=50, null=True,blank=True, default='')
    leader = models.CharField(max_length=50, null=True,blank=True, default='')
    stat = models.SmallIntegerField(choices=STAT, default=10)
    log = models.TextField(default="")
    acts = models.TextField(default="[]")
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        act_list = json.loads(self.acts)
        if not act_list or act_list[-1] != self.stat:
            act_list.append(self.stat)
            self.acts = json.dumps(act_list)
        super(SqlNode, self).save(*args, **kwargs)

class SqlNodeForm(forms.ModelForm):
    class Meta:
        model = SqlNode
        exclude = ['author', 'leader', 'stat', 'create_time', 'update_time', 'log', 'db', 'acts']
    def __init__(self,*args,**kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(SqlNodeForm, self).__init__(*args, **kwargs)


class GlobalAuthority(models.Model):
    COMMON = 1
    SERVER = 2
    SYS = 3
    PROJECT = 4
    APP = 5
    OTHER = 6
    TYPE_CHOICE=(
        (SYS, '系统'),
        (COMMON, '通用'),
        (SERVER, '项目'),
        (OTHER, '其他'),
    )
    name = models.CharField(max_length=50, verbose_name="名称", null=True)
    url = models.CharField(max_length=255, verbose_name="URL", null=True, blank=True)
    type = models.SmallIntegerField(choices=TYPE_CHOICE, default=OTHER)
    parent = models.ForeignKey('self', blank=True,null=True, on_delete=models.CASCADE)
    link_app = models.ForeignKey('AppModel', null=True, blank=True, on_delete=models.CASCADE)
    link_project = models.ForeignKey('ProjectModel', null=True, blank=True, on_delete=models.CASCADE)
    link_db = models.ForeignKey('DbNode', null=True, blank=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.name
    class Meta:
        db_table = 'authority'

class GlobalAuthorityForm(forms.ModelForm):
    class Meta:
        model = GlobalAuthority
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(GlobalAuthorityForm, self).__init__(*args, **kwargs)

class UserAuthority(models.Model):
    user = models.ForeignKey('User',on_delete=models.CASCADE)
    authority = models.ForeignKey('GlobalAuthority',
                                  on_delete=models.CASCADE)
    type = models.SmallIntegerField(null=True, blank=False)

    def save(self, *args, **kwargs):
        self.type = self.authority.type
        super(self.__class__, self).save(*args, **kwargs)

    class Meta:
        db_table = 'userauthority'



## pod监控数据表
class PodMonitor(models.Model):
    name = models.CharField(max_length=50, verbose_name="应用名", null=True)
    cpu = models.CharField(max_length=10, verbose_name="CPU", null=True)
    mem = models.CharField(max_length=10, verbose_name="内存", null=True)
    time = models.DateTimeField(verbose_name="时间", auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'server_podmonitor'


## 公有云信息
class Cmdb(models.Model):
    ALIYUN = 1
    HWYUN = 2
    JSYUN = 3
    AWS = 4
    TYPE_CHOICE=(
        (ALIYUN, '阿里云'),
        (HWYUN, '华为云'),
        (JSYUN, '金山云'),
        (AWS, 'AWS'),
    )
    name = models.CharField(max_length=50, verbose_name='公有云名称', null=False)
    type = models.SmallIntegerField(choices=TYPE_CHOICE, default=ALIYUN)
    access_key_id = models.CharField(max_length=50, verbose_name='access_key_id', null=False)
    access_key_secret = models.CharField(max_length=50, verbose_name='access_key_secret', null=False)
    # region_id = models.CharField(max_length=50, verbose_name='region_id', null=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cmdb'

class CmdbForm(forms.ModelForm):
    class Meta:
        model = Cmdb
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        for field_name in self.base_fields:
            field = self.base_fields[field_name]
            field.widget.attrs.update({'class': 'form-control'})
        super(CmdbForm, self).__init__(*args, **kwargs)








